/*******************************************************************

Archivo de Procedimientos Almacenados (DML) **
********************************************************************/
 /*
    Version:        1.0
    Fecha:          07/09/2020   17:00 pm
    Autor:          Valeria Magdalena Sanchez Gonzalez 
    Email:          valerybtrmagda@gmail.com
    Comentarios:    Vista y procedimientos almacenados para manipular los datos 
					de Administrador.
 */
 USE autoguia;

 /*Procedimiento para insertar los datos de un administrador */
DROP PROCEDURE IF EXISTS insertarAdministrador;
DELIMITER $$
CREATE PROCEDURE insertarAdministrador(
-- Para tabla Usuario 
	IN usuario varchar(20) ,
    IN contrasenia varchar(20),
    out idU int,
    out idA int
    )
BEGIN
INSERT INTO usuarioweb(idUsuario,usuario,contrasenia,token,rol,estatus) VALUES(DEFAULT,usuario,contrasenia,null,"Administrador",1);
set idU=last_insert_id();
INSERT INTO administrador(idAdministrador,idUsuario) values(default,idU);
set idA=last_insert_id();
END $$
-- Prueba insertarAdministrador

-- call insertarAdministrador('pegasso','pegasso',@idU,@idA);

 /*Procedimiento para modificar los datos de un administrador */
DROP PROCEDURE IF EXISTS modificarAdministrador;
DELIMITER $$
CREATE PROCEDURE modificarAdministrador(
-- Para tabla Usuario 
	IN usuario varchar(20) ,
    IN contrasenia varchar(20),
    in idU int,
    in idA int
    )
BEGIN
UPDATE usuarioweb SET 
    usuario=usuario,
    contrasenia=contrasenia
    where idUsuario=idU;
END $$
-- Prueba modificarAdministrador
-- call modificarAdministrador('VALEMALEadminres','valeriaTres',1,1);

/* Vistas */
-- Administrador
-- vista de administrador
DROP view IF EXISTS vista_administrador;
create view vista_administrador as(
select ul.*, a.idAdministrador from administrador a inner join usuariologin ul where a.idUsuario=ul.idUsuario);
-- vista de administrador sin token
DROP view IF EXISTS administradorDatos;
create view administradorDatos as(
select ul.idUsuario,ul.usuario,ul.contrasenia,ul.rol,ul.estatus,a.idAdministrador from administrador a 
inner join usuariologin ul where a.idUsuario=ul.idUsuario);
