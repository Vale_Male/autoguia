/*******************************************************************

Archivo de Procedimientos Almacenados (DML) **
********************************************************************/
 /*
    Version:        1.0
    Fecha:          06/09/2020   17:00 pm
    Autor:          César Andres Soto Cervantes
    Email:          soto.cervantes.cesar@gmail.com
    Comentarios:    Vista y procedimientos almacenados para manipular los datos 
					de Gerente.
 */
  /*Procedimiento para insertar los datos de un gerente */

DROP PROCEDURE IF EXISTS insertarGerente;
DELIMITER $$
CREATE PROCEDURE insertarGerente(  
IN v_nombre             VARCHAR(20),
IN v_apellidoPaterno    VARCHAR(15),
IN v_apellidoMaterno    VARCHAR(15),
IN v_calle              VARCHAR(40),
IN v_colonia            VARCHAR(40),
IN v_numero             VARCHAR(10),
IN v_codigo_postal      INT,
IN v_foto               LONGTEXT,
IN v_fechaNacimiento    DATE,
IN v_fechaInicio        DATE,
IN v_genero             INT,
IN v_telefono           VARCHAR(10),

IN v_usuario            VARCHAR(20),
IN v_contrasenia        VARCHAR(20),
IN v_token              VARCHAR(70),
IN v_rol                VARCHAR(20),

IN v_nombre_ciudad      VARCHAR(20),
IN v_estado             VARCHAR(20)
)
BEGIN
declare var_idP int;
declare var_idU int;
declare var_idC int;
INSERT into persona(nombre ,apellidoPaterno,apellidoMaterno,calle,colonia,numero,codigo_postal,foto,fechaNacimiento,fechaInicio,genero,telefono)
values(v_nombre,v_apellidoPaterno,v_apellidoMaterno,v_calle,v_colonia,v_numero,v_codigo_postal,v_foto,v_fechaNacimiento,v_fechaInicio,v_genero,v_telefono);
SET var_idP = last_insert_id();

INSERT INTO usuarioLogin(usuario,contrasenia,token,rol)
VALUES(v_usuario,v_contrasenia,v_token,v_rol);
SET var_idU = last_insert_id();

INSERT INTO ciudad(nombre_ciudad,estado)
VALUES(v_nombre_ciudad,v_estado); 
SET var_idC = last_insert_id();

INSERT into gerente(idPersona,idUsuario,idCiudad)
values (var_idP,var_idU,var_idC);
END
$$
DELIMITER ;

-- CALL insertarGerente('César','Soto','Cervantes','San vicente de Paúl','Santa Rosa de Lima','301B',37210,'csfhgsrfsgcfcsfdfs','1999-09-18','2020-08-23',1,'4775265006','casc99@gmail.com','123','jfbks764dnsjbc86473nasx','Gerente','León','Guanajuato');

  /*Vista para consultar los datos de un gerente */
  
CREATE VIEW AllGerentes AS 
SELECT g.idGerente,p.idPersona,ul.idUsuario,c.idCiudad,p.nombre,p.apellidoPaterno,p.apellidoMaterno,p.calle,p.colonia,p.numero,p.codigo_postal,
p.foto,p.fechaNacimiento,p.fechaInicio,p.genero,p.telefono,ul.usuario,ul.contrasenia,ul.token,ul.estatus,ul.rol,
c.nombre_ciudad,c.estado FROM gerente as g inner join persona as p on g.idPersona= p.idPersona INNER JOIN usuarioLogin as ul
ON ul.idUsuario = g.idUsuario INNER JOIN ciudad as c ON c.idCiudad = g.idCiudad;

-- select * from AllGerentes;
  /*Procedimiento para eliminar los datos de un gerente */

drop procedure if exists eliminarGerente;
delimiter $$
create procedure eliminarGerente(
idG int
)
begin 
update usuarioLogin set estatus = 0
where idUsuario=idG;
end $$ 
delimiter ;

call eliminarGerente(1);
  /*Procedimiento para activar los datos de un gerente */

drop procedure if exists activarGerente;
delimiter $$
create procedure activarGerente(
idG int
)
begin 
update usuarioLogin set estatus = 1
where idUsuario=idG;
end $$ 
delimiter ;

-- CALL activarGerente(1);

  /*Procedimiento para modificar los datos de un gerente */
DROP PROCEDURE IF EXISTS updateGerentes;
DELIMITER $$
CREATE PROCEDURE updateGerentes( 	
IN v_nombre             VARCHAR(20),
IN v_apellidoPaterno    VARCHAR(15),
IN v_apellidoMaterno    VARCHAR(15),
IN v_calle              VARCHAR(40),
IN v_colonia            VARCHAR(40),
IN v_numero             VARCHAR(10),
IN v_codigo_postal      INT,
IN v_foto               LONGTEXT,
IN v_fechaNacimiento    DATE,
IN v_fechaInicio        DATE,
IN v_genero             INT,
IN v_telefono           VARCHAR(10),

IN v_Id int
                               )
BEGIN
update AllGerentes set nombre=v_nombre ,apellidoPaterno=v_apellidoPaterno, apellidoMaterno=v_apellidoMaterno,
calle=v_calle, colonia=v_colonia, numero=v_numero, codigo_postal=v_codigo_postal, foto=v_foto, fechaNacimiento=v_fechaNacimiento,
fechaInicio=v_fechaInicio, genero=v_genero, telefono=v_telefono
where idGerente=v_Id;
 END $$
DELIMITER ;
-- select*from AllGerentes;
-- call updateGerentes('Andres','Soto','Cervantes','San vicente de Paúl','Santa Rosa de Lima','301B',37210,'csfhgsrfsgcfcsfdfs','1999-09-18','2020-08-23',1,'4775265006',1);