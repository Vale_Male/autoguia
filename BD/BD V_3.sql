/************************************************
 *      BASE DE DATOS Autoguia                *
 *                                              *
 *      Archivo de Definicion de Datos (DDL)    *
 ***********************************************/
 
 /*
    Version:        1.1
    Fecha:          03/09/2020 23:24:00
    Autor:          Valeria Magdalena Sanchez Gonzalez
    Email:          valerybtrmagda@gmail.com
    Comentarios:    Esta es la primera version de la base de datos.
    
    -- César Soto: Cambie a varchar el campo numero 05/09/2020 23:24:00
    -- César Soto: Agregue int not null default 1 05/09/2020 23:24:00
    -- Eduardo: se agrego el idVendedor y el idGerente paso a ser una FK 05/09/2020 23:24:00
    -- Eduardo: correccion del nombre en el id 05/09/2020 23:24:00
	-- César Soto: Cambie a varchar el campo numero y agregue el campo estatus 05/09/2020 23:24:00

 */
 
DROP DATABASE IF EXISTS autoguia;

CREATE DATABASE autoguia;

USE autoguia;
  drop table IF EXISTS persona;

CREATE TABLE persona(
	idPersona INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre varchar(20),
    apellidoPaterno varchar(15),
    apellidoMaterno varchar(15),
    calle varchar(40),
    colonia varchar(40),
    numero varchar(10),
    codigo_postal int,
    foto longtext,
    fechaNacimiento date,
    fechaInicio date,
    genero int,-- 1 masculino, 2 femenino, 3 otro
    telefono varchar(10)
);
  drop table IF EXISTS usuarioLogin;

Create table usuarioLogin(
    idUsuario INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    usuario varchar(20),
    contrasenia varchar(20),
    token varchar(70),
    estatus int not null default 1, -- 1 activo, 0 inactivo
    rol varchar(20)
    );
  drop table IF EXISTS administrador;

CREATE TABLE administrador(
	idAdministrador INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idUsuario int,
    usuario varchar(20),
    contrasenia varchar(20),
    token varchar(70),
    estatus int, -- 1 activo, 0 inactivo
    CONSTRAINT  fk_administrador_usuario  FOREIGN KEY (idUsuario) 
                REFERENCES usuarioLogin(idUsuario) ON DELETE CASCADE ON UPDATE CASCADE
);
  drop table IF EXISTS ciudad;

CREATE TABLE ciudad(
	idCiudad INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre_ciudad varchar(20),
    estado varchar(20)
    );
      drop table IF EXISTS zona;

CREATE TABLE zona(
	idZonas INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idCiudad int,
    nombre_zona varchar(20),
      CONSTRAINT  fk_zona_ciudad  FOREIGN KEY (idCiudad) 
                REFERENCES ciudad(idCiudad) ON DELETE CASCADE ON UPDATE CASCADE
    );

  drop table IF EXISTS gerente;

CREATE TABLE gerente(
	idGerente INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idUsuario int,
    idCiudad int,
    idPersona int,
    CONSTRAINT  fk_gerenter_usuario  FOREIGN KEY (idUsuario) 
                REFERENCES usuarioLogin(idUsuario) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT  fk_gerenter_ciudad  FOREIGN KEY (idCiudad) 
                REFERENCES ciudad(idCiudad) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT  fk_gerenter_persona  FOREIGN KEY (idPersona) 
                REFERENCES persona(idPersona) ON DELETE CASCADE ON UPDATE CASCADE
);
  drop table IF EXISTS vendedor;
CREATE TABLE vendedor(
	idVendedor INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idGerente int,
    idUsuario int,
    idZonas int,
    idPersona int,
    CONSTRAINT  fk_vendedor_gerente  FOREIGN KEY (idGerente) 
                REFERENCES gerente(idGerente) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT  fk_vendedor_usuario  FOREIGN KEY (idUsuario) 
                REFERENCES usuarioLogin(idUsuario) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT  fk_vededor_zona  FOREIGN KEY (idZonas) 
                REFERENCES zona(idZonas) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT  fk_vendedor_persona  FOREIGN KEY (idPersona) 
                REFERENCES persona(idPersona) ON DELETE CASCADE ON UPDATE CASCADE
);
  drop table IF EXISTS anunciante;

CREATE TABLE anunciante(
	idAnunciante INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre_comercial varchar(50),
    logo longtext,
    imagen_promocion longtext,
    mas_imagenes int,
    descripcion longtext,
    cotizacion int,
	calle varchar(40),
    colonia varchar(40),
    numero varchar(10),
    codigo_postal int,
    estatus int not null default 1
   );
   
  drop table IF EXISTS filtros;

   create table filtros(
   idFiltro INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
   fltro varchar(20)
   );
  drop table IF EXISTS filtro_anunciante;

  create table filtro_anunciante(
   idFil INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
   idAnunciante int,
   CONSTRAINT  fk_filtro_anunciante  FOREIGN KEY (idAnunciante) 
                REFERENCES anunciante(idAnunciante) ON DELETE CASCADE ON UPDATE CASCADE
);
drop table IF EXISTS imagenes;

   create table imagenes(
   idImagenes INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
   imagen longtext,
   idAnunciante int,
   CONSTRAINT  fk_imagen_anunciante  FOREIGN KEY (idAnunciante) 
                REFERENCES anunciante(idAnunciante) ON DELETE CASCADE ON UPDATE CASCADE
);
	drop table IF EXISTS promociones;
  create table promociones(
   idPromocion INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
   imagen_base longtext,
   descripcion longtext,
   vigencia_inicio date,
   vigencia_fin date,
   servicio_domicilio int,-- 1 si, 2 no
   idAnunciante int,
   CONSTRAINT  fk_promo_anunciante  FOREIGN KEY (idAnunciante) 
                REFERENCES anunciante(idAnunciante) ON DELETE CASCADE ON UPDATE CASCADE
);
drop table IF EXISTS horario;

CREATE TABLE horario(
	idHorario INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idAnunciante INT,
    dia VARCHAR(20),
    horaInicio TIME,
    horaFin TIME,
    CONSTRAINT  fk_horario_anunciante  FOREIGN KEY (idAnunciante) 
                REFERENCES anunciante(idAnunciante) ON DELETE CASCADE ON UPDATE CASCADE
);
drop table IF EXISTS productos;

CREATE TABLE productos(
	idProducto INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50),
    precio int,
    existencias int,
    foto longtext
    );
    
drop table IF EXISTS usuario;
CREATE TABLE usuario(
	idUsuario INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    usuario VARCHAR(20) unique,
    contrasenia VARCHAR(20)
);