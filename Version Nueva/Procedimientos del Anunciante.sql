DROP PROCEDURE insertarAnunciante;
DELIMITER $$
CREATE PROCEDURE insertarAnunciante(
IN v_nombre_comercial     VARCHAR(50),
IN v_logo                 LONGTEXT,
IN v_mas_imagenes         INT,
IN v_descripcion          LONGTEXT,
IN v_cotizacion           INT,
IN v_calle                VARCHAR(40),
IN v_colonia              VARCHAR(40),
IN v_numero               VARCHAR(10),
IN v_codigo_postal        INT,

IN v_imagen               LONGTEXT,

IN v_idV INT
)BEGIN
DECLARE var_idA INT;
INSERT INTO anunciante(idVendedor,nombre_comercial,logo,mas_imagenes,descripcion,cotizacion,
calle,colonia,numero,codigo_postal)
VALUES(v_idV,v_nombre_comercial,v_logo,v_mas_imagenes,v_descripcion,v_cotizacion,v_calle,v_colonia,
v_numero,v_codigo_postal);

INSERT INTO imagenes(idAnunciante,imagen)
VALUES(var_idA,v_imagen);
SET var_idA = last_insert_id();
END $$
DELIMITER ;

Call insertarAnunciante('Pegasso','dkcsdkr4ekwdcnz3enk',2,'Autopartes',2500,'Oxígeno',
'Valle de Señora','205A',34627,'xasjdy7a6s7ayushxaj7',1);

/*Vista para mostrar información del anunciante*/
DROP VIEW IF EXISTS vistaAnunciante;
create view vistaAnunciante as
select a.idAnunciante,p.idPersona, p.nombre, p.apellidoPaterno, p.apellidoMaterno, p.calle, p.colonia, p.numero, p.codigo_postal, 
p.foto, p.fechaNacimiento, p.fechaInicio, p.genero, p.telefono, u.usuario, u.contrasenia, u.rol, u.estatus, 
z.idZonas, z.nombre_zona, v.idVendedor, a.nombre_comercial, a.logo, a.mas_imagenes,a.descripcion,a.cotizacion,
a.calle as a_calle,a.colonia as a_colonia,a.numero as a_numero, a.codigo_postal as a_codigo_postal,a.estatus as a_estatus,
i.imagen from vendedor as v inner join gerente as g on v.idGerente=g.idGerente inner join usuariologin as u 
on v.idUsuario=u.idUsuario inner join zona as z on v.idZonas=z.idZonas inner join persona as p 
on v.idPersona=p.idPersona inner join anunciante as a on v.idVendedor = a.idVendedor inner join imagenes as i 
on i.idAnunciante=a.idAnunciante;
 select * from vistaAnunciante;
 
 select * from anunciante;

/*Eliminar Anunciante*/
drop procedure if exists eliminarAnunciante;
delimiter $$
create procedure eliminarAnunciante(
idA int
)
begin 
delete from anunciante
where idAnunciante=idA;
end $$ 
delimiter ;
call eliminarAnunciante(1); 


/*Actualizar información del anunciante*/
DROP PROCEDURE IF EXISTS updateAnunciantes;
DELIMITER $$
CREATE PROCEDURE updateAnunciantes( 	
IN v_nombre_comercial     VARCHAR(50),
IN v_logo                 LONGTEXT,
IN v_mas_imagenes         INT,
IN v_descripcion          LONGTEXT,
IN v_cotizacion           INT,
IN v_calle                VARCHAR(40),
IN v_colonia              VARCHAR(40),
IN v_numero               VARCHAR(10),
IN v_codigo_postal        INT,

IN v_Id int
)
BEGIN
update anunciante set nombre_comercial=v_nombre_comercial ,logo=v_logo, mas_imagenes=v_mas_imagenes, 
descripcion=v_descripcion, cotizacion=v_cotizacion, calle=v_calle, colonia=v_colonia, numero=v_numero, 
codigo_postal=v_codigo_postal where idAnunciante=v_Id;
 END $$
DELIMITER ;

Call updateAnunciantes('Pegasso','dkcsdkr4ekwdcnz3enk',2,'Motopartes',2500,'Oro','Valle de Señora','205A',34627,1);
