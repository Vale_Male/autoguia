/*******************************************************************

Archivo de Procedimientos Almacenados (DML) **
********************************************************************/
 /*
    Version:        1.0
    Fecha:          12/10/2020   17:00 pm
    Autor:          Servin Sotelo Jose Eduardo
    Email:          jservinsotelo14@gmail.com
    Comentarios:    Vista y procedimientos almacenados para manipular los datos 
					de Usuario.
 */
USE autoguia;
/* Agregar cliente */
DROP PROCEDURE IF EXISTS agregar_cliente;
Delimiter $$
Create procedure agregar_cliente(
in nombre varchar(20),
in apellidoPaterno varchar(15),
in apellidoMaterno varchar(15),
in calle varchar(40),
in colonia varchar(40),
in numero varchar(10),
in codigo_postal int,
in foto longtext,
in fechaNacimiento date,
in fechaInicio date,
in genero int,
in telefono varchar(10),
in nombreUsuario varchar(20),
in contrasena varchar(20),
in correo varchar(100),
out idP int,
out idU int,
out idC int
)
begin
if((select nombreUsuario from usuario where nombreUsuario=usuario) is null) then
insert into persona values(default, nombre, apellidoPaterno, apellidoMaterno, calle, colonia, numero, codigo_postal, foto, fechaNacimiento, fechaInicio, genero, telefono);
set idP=last_insert_id();
insert into usuario values(default, nombreUsuario, contrasena);
set idU=last_insert_id();
insert into cliente values(default, correo, 1, idP, idU);
set idC=last_insert_id();
select 'Usuario agregado de manera exitosa' as mensaje;
else
set idC=0;
end if;
end
$$
Delimiter ;

call agregar_cliente('Jose Eduardo', 'Servin', 'Sotelo', 'Luis long', 'San Martin de Porres', '203', 37350, 'foto', '2000-02-28', now(), 1, '4775369764', 'jservinsotelo20', '123456789', 'jservinsotelo14@gmail.com', @idP, @idU, @idC);

/* Vista vendedor */
DROP VIEW IF EXISTS v_cliente;
create view v_cliente as
select p.idPersona, p.nombre, p.apellidoPaterno, p.apellidoMaterno, p.calle, p.colonia, p.numero, p.codigo_postal, p.foto, p.fechaNacimiento, p.fechaInicio, p.genero, p.telefono,
u.idUsuario, u.usuario, u.contrasenia, c.idCliente, c.correo, c.estatus from cliente as c
inner join usuario as u on c.idUsuario=u.idUsuario inner join persona as p on c.idPersona=p.idPersona;

select * from v_cliente where estatus=1;
