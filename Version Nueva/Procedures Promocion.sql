/*******************************************************************

Archivo de Procedimientos Almacenados (DML) **
********************************************************************/
 /*
    Version:        1.0
    Fecha:          06/09/2020   17:00 pm
    Autor:          Jose Eduardo Servin Sotelo 
    Email:          jservinsotelo14@gmail.com
    Comentarios:    Vista y procedimientos almacenados para manipular los datos 
					de Promocion
 */
USE autoguia;
/* Agregar promocion */
DROP PROCEDURE IF EXISTS agregar_promocion;
Delimiter $$
create procedure agregar_promocion(
in idPromocion int,
in imagen_base longtext,
in descripcion longtext,
in vigencia_inicio date,
in vigencia_fin date,
in servicio_domicilio int,
in idAnunciante int
)
begin
insert into promociones values(default, imagen_base, descripcion, vigencia_inicio, vigencia_fin, servicio_domicilio, idAnunciante);
set idPromocion = last_insert_id();
end
$$
Delimiter ;

call agregar_promocion(@last_insert_id,'imagen_base', 'descripcion', now(), '2020-09-12', 1, 9);

call agregar_promocion(@last_insert_id,'img', 'Cremallera nueva', now(), '2020-12-12', 2,12);


/* Vista promocion */
drop view if exists v_promocion;
create view v_promocion as
select promociones.idPromocion, promociones.imagen_base, promociones.descripcion, promociones.vigencia_inicio, 
promociones.vigencia_fin, promociones.servicio_domicilio, anunciante.idAnunciante, anunciante.nombre_comercial, 
anunciante.logo, anunciante.mas_imagenes, anunciante.descripcion as AnuncianteDes, anunciante.cotizacion, 
anunciante.calle, anunciante.colonia, anunciante.numero, anunciante.codigo_postal, i.imagen from promociones 
inner join anunciante on promociones.idAnunciante= anunciante.idAnunciante inner join imagenes as i 
on i.idAnunciante=anunciante.idAnunciante;

select * from v_promocion;

/* Eliminar Promocion */
drop procedure if exists eliminarPromocion;
delimiter $$
create procedure eliminarPromocion(
in idPR int
)
begin 
delete from promociones where idPromocion=idPR;
end
$$ 
delimiter ;

call eliminarPromocion(2);
select * from v_promocion;

/* Actualizar promocion */
drop procedure if exists actualizarPromocion;
Delimiter $$
create procedure actualizarPromocion(
in idPr int,
in v_imagen_base longtext,
in v_descripcion longtext,
in v_vigencia_inicio date,
in v_vigencia_fin date,
in v_servicio_domicilio int
)
begin
update promociones set imagen_base=v_imagen_base, descripcion=v_descripcion, vigencia_inicio=v_vigencia_inicio,
vigencia_fin=v_vigencia_fin, servicio_domicilio=v_servicio_domicilio where idPromocion=idPr;
end
$$
Delimiter ;

call actualizarPromocion(1, 'imagen', 'Descripción', now(), '2020/09/10', 1);
select * from v_promocion;