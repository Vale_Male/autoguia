/*******************************************************************

Archivo de Procedimientos Almacenados (DML) **
********************************************************************/
 /*
    Version:        1.0
    Fecha:          06/09/2020   17:00 pm
    Autor:          Jose Eduardo Servin Sotelo 
    Email:          jservinsotelo14@gmail.com
    Comentarios:    Vista y procedimientos almacenados para manipular los datos 
					de Vendedor.
 */
USE autoguia;
/* Agregar vendedor */
DROP PROCEDURE IF EXISTS agregar_vendedor;
Delimiter $$
Create procedure agregar_vendedor(
in nombre varchar(20),
in apellidoPaterno varchar(15),
in apellidoMaterno varchar(15),
in calle varchar(40),
in colonia varchar(40),
in numero varchar(10),
in codigo_postal int,
in foto longtext,
in fechaNacimiento date,
in fechaInicio date,
in genero int,
in telefono varchar(10),
in usuario varchar(20),
in contrasenia varchar(20),
in token varchar(70),
in estatus int,
in rol varchar(20),
in nombre_zona varchar(20),
in nombre_ciudad varchar(20),
in estado varchar(20),
out idP int,
out idV int,
out idG int,
out idU int,
out idZ int,
out idC int
)
begin
insert into persona values(default, nombre, apellidoPaterno, apellidoMaterno, calle, colonia, numero, codigo_postal, foto, fechaNacimiento, fechaInicio, genero, telefono);
set idP=last_insert_id();
insert into usuariologin values(default, usuario, contrasenia, '', 1, rol);
set idU=last_insert_id();
insert into ciudad values(default, nombre_ciudad, estado);
set idC=last_insert_id();
insert into zona values(default, idC, nombre_zona);
set idZ=last_insert_id();
insert into gerente values(default, idU, idC, idP);
set idG=last_insert_id();
insert into vendedor values(default, idG, idU, idZ, idP);
set idV=last_insert_id();
end
$$
Delimiter ;

call agregar_vendedor('nombre', 'apellidoP', 'apellidoM', 'calle', 'colonia', 'numero', 37000, 'foto', '2000-02-28', now(), 1, 4775369764, 'usuario', 'contrasena', ' ', 1, 'rol', 'nombre_ciudad', 'estado', 'nombre_zona', @idP, @idU, @idC, @idZ, @idG, @idV);

/* Vista vendedor */
DROP VIEW IF EXISTS v_vendedor;
create view v_vendedor as
select p.idPersona, p.nombre, p.apellidoPaterno, p.apellidoMaterno, p.calle, p.colonia, p.numero, p.codigo_postal, p.foto, p.fechaNacimiento, p.fechaInicio, p.genero, p.telefono,
u.usuario, u.contrasenia, u.rol, u.estatus, z.idZonas, z.nombre_zona, v.idVendedor from vendedor as v
inner join gerente as g on v.idGerente=g.idGerente inner join usuariologin as u on v.idUsuario=u.idUsuario inner join zona as z on v.idZonas=z.idZonas inner join persona as p on v.idPersona=p.idPersona;

select * from v_vendedor where estatus=1;

select * from v_vendedor where estatus=0;

/* Modificar vendedor */
DROP PROCEDURE IF EXISTS modificar_vendedor;
Delimiter $$
create procedure modificar_vendedor(
in idP int,
in v_nombre varchar(20),
in v_apellidoPaterno varchar(15),
in v_apellidoMaterno varchar(15),
in v_calle varchar(40),
in v_colonia varchar(40),
in v_numero varchar(10),
in v_codigo_postal int,
in v_foto longtext,
in v_fechaNacimiento date,
in v_genero int,
in v_telefono varchar(10)
)
begin
update persona set nombre =v_nombre, apellidoPaterno=v_apellidoPaterno, apellidoMaterno=v_apellidoMaterno, calle=v_calle, colonia=v_colonia, numero=v_numero, codigo_postal=v_codigo_postal, foto=v_foto, fechaNacimiento=v_fechaNacimiento, genero=v_genero, telefono=v_telefono where idPersona=idP;
end
$$
Delimiter ;

-- call modificar_vendedor(5,'nombre', 'p', 'm', 'c', 'col', 'nu', 37000, 'foto', '2000-02-28', '3', '4775369764');

/* Eliminar vendedor */
drop procedure if exists eliminarVendedor;
delimiter $$
create procedure eliminarVendedor(
in idV int
)
begin 
update usuarioLogin set estatus = 0
where idUsuario=idV;
end
$$ 
delimiter ;
-- call eliminarVendedor(5);

/* Activar vendedor */
drop procedure if exists activarVendedor;
delimiter $$
create procedure activarVendedor(
in idV int
)
begin 
update usuarioLogin set estatus = 1
where idUsuario=idV;
end
$$ 
delimiter ;
-- call activarVendedor(2);