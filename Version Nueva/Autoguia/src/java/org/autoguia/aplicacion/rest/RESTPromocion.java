/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.rest;

import com.google.gson.Gson;
import java.util.ArrayList;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.autoguia.aplicacion.control.ControlPromocion;
import org.autoguia.aplicacion.modelo.Anunciante;
import org.autoguia.aplicacion.modelo.Promociones;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 05/10/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 05/10/2020 Rest
 */
@Path("promociones")
public class RESTPromocion {
    
    @GET
    @Path("getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        ArrayList<Promociones> promociones = null;
        Gson json = new Gson();

        ControlPromocion ctrlP = new ControlPromocion();

        String salida = null;
        try {
            promociones = ctrlP.selectAll();
            salida = json.toJson(promociones);
        } catch (Exception e) {
            e.printStackTrace();
            salida = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(salida).build();
    }
    
    @POST
    @Path("insert")
    @Produces(MediaType.APPLICATION_JSON)
    public Response insert(
            @FormParam("imagen_base") String imagen_base,
            @FormParam("descripcion") String descripcion,
            @FormParam("vigencia_inicio") String vigencia_inicio,
            @FormParam("vigencia_fin") String vigencia_fin,
            @FormParam("servicio_domicilio") int servicio_domicilio,
            @FormParam("anunciante") int anunciante
    ) {
        Anunciante objA = new Anunciante();
        objA.setIdAnunciante(anunciante);

        Promociones objP = new Promociones(0, imagen_base, descripcion, vigencia_inicio, vigencia_fin, servicio_domicilio, objA);

        ControlPromocion objCP = new ControlPromocion();
        String out = null;

        try {
            objCP.insert(objP);
            out = "{\"result\":\"OK\"}";
        } catch (Exception err) {
            out = "{\"error\":\"ERROR\"}";
            err.printStackTrace();
        }

        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(
            @FormParam("idPromocion") @DefaultValue("0") int idPromocion,
            @FormParam("imagen_base") String imagen_base,
            @FormParam("descripcion") String descripcion,
            @FormParam("vigencia_inicio") String vigencia_inicio,
            @FormParam("vigencia_fin") String vigencia_fin,
            @FormParam("servicio_domicilio") int servicio_domicilio
    ) {

        Promociones objP = new Promociones(idPromocion, imagen_base, descripcion, vigencia_inicio, vigencia_fin, servicio_domicilio);
        ControlPromocion ctrlP = new ControlPromocion();
        String salida = null;
        try {
            ctrlP.update(objP);
            salida = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            salida = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(salida).build();
    }
    
    @POST
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@FormParam("idPromocion") @DefaultValue("0") int idPromocion) {

        ControlPromocion ctrlP = new ControlPromocion();

        String salida = null;
        try {
            ctrlP.delete(idPromocion);
            salida = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            salida = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(salida).build();
    }
}
