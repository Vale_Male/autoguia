/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.rest;

import com.google.gson.Gson;
import java.util.ArrayList;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.autoguia.aplicacion.control.ControlAnunciante;
import org.autoguia.aplicacion.modelo.Anunciante;


/**
 *
 * @author guita
 */
@Path("Anunciante")
public class RESTAnunciante extends Application{
    @GET
    @Path("getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        ControlAnunciante ca = new ControlAnunciante();
        ArrayList<Anunciante> anunciantes = null;
        Gson gson = new Gson();

        String out = null;
        try {
            anunciantes = ca.selectAll();
            out = gson.toJson(anunciantes);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @POST
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(
            @FormParam("idAnunciante") @DefaultValue("1") int idAnunciante) throws Exception{
            
        String out = "";
        ControlAnunciante ca = new ControlAnunciante();
        
        if(ca.deleteAnunciante(idAnunciante)){
                out = "{\"result\":\"OK\"}";
            }
            else {
                out = "{\"error\":\"ERROR\"}";
            }
        
       
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(
        @FormParam("nombre_comercial") @DefaultValue("N/A") String nombre_comercial,
        @FormParam("logo") @DefaultValue("N/A") String logo,
        @FormParam("mas_imagenes") @DefaultValue("0") int mas_imagenes,
        @FormParam("descripcion") @DefaultValue("N/A") String descripcion,
        @FormParam("cotizacion") @DefaultValue("00") int cotizacion,
        @FormParam("calle") @DefaultValue("N/A") String calle,
        @FormParam("colonia") @DefaultValue("N/A") String colonia,
        @FormParam("numero") @DefaultValue("N/A") String numero,
        @FormParam("codigo_postal") @DefaultValue("00000") int codigo_postal,
        @FormParam("idAnunciante") @DefaultValue("0") int idAnunciante) throws Exception {
            
        ControlAnunciante ca = new ControlAnunciante();
        Anunciante anunciante = new Anunciante();
        anunciante.setNombre_comercial(nombre_comercial);
        anunciante.setLogo(logo);
        anunciante.setMas_imagenes(mas_imagenes);
        anunciante.setDescripcion(descripcion);
        anunciante.setCotizacion(cotizacion);
        anunciante.setCalle(calle);
        anunciante.setColonia(colonia);
        anunciante.setNumero(numero);
        anunciante.setCodigo_postal(codigo_postal);
        anunciante.setIdAnunciante(idAnunciante);
       String out = "";
        
       
            if(ca.updateAnunciante(anunciante)){
                out = "{\"result\":\"OK\"}";
            }
            else {
                out = "{\"error\":\"ERROR\"}";
            }
       
        return Response.status(Response.Status.OK).entity(out).build();
    }
}
