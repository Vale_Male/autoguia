/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.rest;

import com.google.gson.Gson;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.autoguia.aplicacion.control.ControlUsuario;
import org.autoguia.aplicacion.modelo.Cliente;
import org.autoguia.aplicacion.modelo.Persona;
import org.autoguia.aplicacion.modelo.Usuario;

/**
 *
 * @author jose-
 */
@Path("usuario")
public class RESTUsuario {
    @POST
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("validar")
    public Response verificarUsuario(@FormParam("user") String usuario, 
                                     @FormParam("password") String pass){
        ControlUsuario cu = new ControlUsuario();
        boolean respuesta;
        String json="";
        try {
            respuesta=cu.validarUsuario(usuario, pass);
            Gson gson = new Gson();
            json="{\"validado\":"+gson.toJson(respuesta)+"}";
        } catch (Exception ex) {
            json="{\"error\":\""+ex.toString()+"\"}";
        }
        return Response.status(Response.Status.OK).entity(json).build();
    }
}
