/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.control;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import org.autoguia.aplicacion.modelo.Ciudad;
import org.autoguia.aplicacion.modelo.Persona;
import org.autoguia.aplicacion.modelo.UsuarioLogin;
import org.autoguia.aplicacion.modelo.Vendedor;
import org.autoguia.aplicacion.modelo.Zona;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 21/09/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 22/09/2020 Controlador
 */
public class ControlVendedor {
    
    public ArrayList<Vendedor> selectAll() throws Exception {
        String sql = "select * from v_vendedor where estatus=1;";
        ConexionMySQL c = new ConexionMySQL();
        ArrayList<Vendedor> vendedor = new ArrayList<>();
        try {
            c.abrir();
            Connection conn = c.getConexion();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Persona objP = new Persona();
                objP.setIdPersona(rs.getInt(1));
                objP.setNombre(rs.getString(2));
                objP.setApellidoPaterno(rs.getString(3));
                objP.setApellidoMaterno(rs.getString(4));
                objP.setCalle(rs.getString(5));
                objP.setColonia(rs.getString(6));
                objP.setNumero(rs.getString(7));
                objP.setCodigo_postal(rs.getInt(8));
                objP.setFoto(rs.getString(9));
                objP.setFechaNacimiento(rs.getString(10));
                objP.setFechaInicio(rs.getString(11));
                objP.setGenero(rs.getInt(12));
                objP.setTelefono(rs.getString(13));
                
                UsuarioLogin objU = new UsuarioLogin();
                objU.setUsuario(rs.getString(14));
                objU.setContrasenia(rs.getString(15));
                objU.setRol(rs.getString(16));
                objU.setEstatus(rs.getInt(17));
                
                Zona objZ= new Zona();
                objZ.setIdZonas(rs.getInt(18));
                objZ.setNombre_zona(rs.getString(19));
                
                Vendedor objV= new Vendedor();
                objV.setIdVendedor(rs.getInt(20));
                objV.setPersona(objP);
                objV.setUsuario(objU);
                objV.setZonas(objZ);
                vendedor.add(objV);
            }
            rs.close();
            stmt.close();
            c.cerrar();
        }catch (Exception e) {
            System.out.println(e.toString());
        }
        return vendedor;
    }
    
    public ArrayList<Vendedor> selectAllDelete() throws Exception {
        String sql = "select * from v_vendedor where estatus=0;";
        ConexionMySQL c = new ConexionMySQL();
        ArrayList<Vendedor> vendedor = new ArrayList<>();
        try {
            c.abrir();
            Connection conn = c.getConexion();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Persona objP = new Persona();
                objP.setIdPersona(rs.getInt(1));
                objP.setNombre(rs.getString(2));
                objP.setApellidoPaterno(rs.getString(3));
                objP.setApellidoMaterno(rs.getString(4));
                objP.setCalle(rs.getString(5));
                objP.setColonia(rs.getString(6));
                objP.setNumero(rs.getString(7));
                objP.setCodigo_postal(rs.getInt(8));
                objP.setFoto(rs.getString(9));
                objP.setFechaNacimiento(rs.getString(10));
                objP.setFechaInicio(rs.getString(11));
                objP.setGenero(rs.getInt(12));
                objP.setTelefono(rs.getString(13));
                
                UsuarioLogin objU = new UsuarioLogin();
                objU.setUsuario(rs.getString(14));
                objU.setContrasenia(rs.getString(15));
                objU.setRol(rs.getString(16));
                objU.setEstatus(rs.getInt(17));
                
                Zona objZ= new Zona();
                objZ.setIdZonas(rs.getInt(18));
                objZ.setNombre_zona(rs.getString(19));
                
                Vendedor objV= new Vendedor();
                objV.setIdVendedor(rs.getInt(20));
                objV.setPersona(objP);
                objV.setUsuario(objU);
                objV.setZonas(objZ);
                vendedor.add(objV);
            }
            rs.close();
            stmt.close();
            c.cerrar();
        }catch (Exception e) {
            System.out.println(e.toString());
        }
        return vendedor;
    }
    
    public void insert(Vendedor v) throws Exception {
        String sql = "call  agregar_vendedor(?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?,?)";

        CallableStatement cstmt = null;
        ConexionMySQL objConn = new ConexionMySQL();
        Connection conn = null;
        System.out.println(sql);
        try {
            conn = objConn.abrir();

            cstmt = conn.prepareCall(sql);
            cstmt.setString(1, v.getPersona().getNombre());
            cstmt.setString(2, v.getPersona().getApellidoPaterno());
            cstmt.setString(3, v.getPersona().getApellidoMaterno());
            cstmt.setString(4, v.getPersona().getCalle());
            cstmt.setString(5, v.getPersona().getColonia());
            cstmt.setString(6, v.getPersona().getNumero());
            cstmt.setInt(7, v.getPersona().getCodigo_postal());
            cstmt.setString(8, v.getPersona().getFoto());
            cstmt.setString(9, v.getPersona().getFechaNacimiento());
            cstmt.setString(10, v.getPersona().getFechaInicio());
            cstmt.setInt(11, v.getPersona().getGenero());
            cstmt.setString(12, v.getPersona().getTelefono());
            
            cstmt.setString(13, v.getUsuario().getUsuario());
            cstmt.setString(14, v.getUsuario().getContrasenia());
            cstmt.setString(15, v.getUsuario().getToken());
            cstmt.setInt(16, v.getUsuario().getEstatus());
            cstmt.setString(17, v.getUsuario().getRol());
            cstmt.setString(18, v.getZonas().getCiudad().getNombre_ciudad());
            cstmt.setString(19, v.getZonas().getCiudad().getEstado());
            cstmt.setString(20, v.getZonas().getNombre_zona());
            
            cstmt.registerOutParameter(21, Types.INTEGER);
            cstmt.registerOutParameter(22, Types.INTEGER);
            cstmt.registerOutParameter(23, Types.INTEGER);
            cstmt.registerOutParameter(24, Types.INTEGER);
            //cstmt.registerOutParameter(25, Types.INTEGER);
            cstmt.registerOutParameter(26, Types.INTEGER);
            cstmt.executeUpdate();
            v.getPersona().setIdPersona(cstmt.getInt(21));
            v.getUsuario().setIdUsuario(cstmt.getInt(22));
            v.getZonas().getCiudad().setIdCiudad(cstmt.getInt(23));
            v.getZonas().setIdZonas(cstmt.getInt(24));
            //v.getGerente().setIdGerente(cstmt.getInt(25));
            v.setIdVendedor(cstmt.getInt(26));

            cstmt.close();
            objConn.cerrar();
        } catch (Exception ex) {
            if (cstmt != null) {
                System.out.println("Error de insertar vendedor: " + ex.toString());
                cstmt.close();
            }
            objConn.cerrar();

            throw ex;
        }
    }
    
    public void update(Vendedor v) throws Exception {
        String sql = "call modificar_vendedor(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        CallableStatement cstmt = null;
        ConexionMySQL objConn = new ConexionMySQL();
        Connection conn = null;
        System.out.println(sql);
        try {
            conn = objConn.abrir();
            cstmt = conn.prepareCall(sql);

            
            cstmt.setInt(1, v.getPersona().getIdPersona());
            cstmt.setString(2, v.getPersona().getNombre());
            cstmt.setString(3, v.getPersona().getApellidoPaterno());
            cstmt.setString(4, v.getPersona().getApellidoMaterno());
            cstmt.setString(5, v.getPersona().getCalle());
            cstmt.setString(6, v.getPersona().getColonia());
            cstmt.setString(7, v.getPersona().getNumero());
            cstmt.setInt(8, v.getPersona().getCodigo_postal());
            cstmt.setString(9, v.getPersona().getFoto());
            cstmt.setString(10, v.getPersona().getFechaNacimiento());
            cstmt.setInt(11, v.getPersona().getGenero());
            cstmt.setString(12, v.getPersona().getTelefono());

            cstmt.executeUpdate();

            cstmt.close();
            objConn.cerrar();
        } catch (Exception ex) {
            if (cstmt != null) {
                cstmt.close();
            }
            objConn.cerrar();
            throw ex;
        }
    }
    
    public void delete(int idVendedor) throws Exception {
        String sql = "call eliminarVendedor(?)";

        CallableStatement cstmt = null;
        ConexionMySQL objConn = new ConexionMySQL();
        Connection conn = null;
        try {
            conn = objConn.abrir();
            cstmt = conn.prepareCall(sql);
            cstmt.setInt(1, idVendedor);
            cstmt.executeUpdate();

            cstmt.close();
            objConn.cerrar();
        } catch (Exception ex) {
            if (cstmt != null) {
                cstmt.close();
            }

            objConn.cerrar();
            throw ex;
        }
    }
    
    public void active(int idVendedor) throws Exception {
        String sql = "call activarVendedor(?)";

        CallableStatement cstmt = null;
        ConexionMySQL objConn = new ConexionMySQL();
        Connection conn = null;
        try {
            conn = objConn.abrir();
            cstmt = conn.prepareCall(sql);
            cstmt.setInt(1, idVendedor);
            cstmt.executeUpdate();

            cstmt.close();
            objConn.cerrar();
        } catch (Exception ex) {
            if (cstmt != null) {
                cstmt.close();
            }

            objConn.cerrar();
            throw ex;
        }
    }
    
}
