/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.control;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.autoguia.aplicacion.modelo.Anunciante;
import org.autoguia.aplicacion.modelo.Imagenes;
import org.autoguia.aplicacion.modelo.Persona;
import org.autoguia.aplicacion.modelo.UsuarioLogin;
import org.autoguia.aplicacion.modelo.Vendedor;
import org.autoguia.aplicacion.modelo.Zona;

/**
 *
 * @author jose-
 */
public class ControlAnunciante {
    public boolean insertAnunciante(Anunciante anunciante){
        ConexionMySQL conexion = new ConexionMySQL();
        boolean resp= false; 
        try {
            Connection c = conexion.abrir();
            String sql = "call insertarAnunciante(?,?,?,?,?,?,?,?,?,?,?,?);";
            try (CallableStatement stmt = (CallableStatement) c.prepareCall(sql)) {
                stmt.setString(1, anunciante.getNombre_comercial());
                stmt.setString(2, anunciante.getLogo());
                stmt.setInt(3, anunciante.getMas_imagenes());
                stmt.setString(4, anunciante.getDescripcion());
                stmt.setInt(5, anunciante.getCotizacion());
                stmt.setString(6, anunciante.getCalle());
                stmt.setString(7, anunciante.getColonia());
                stmt.setString(8, anunciante.getNumero());
                stmt.setInt(9, anunciante.getCodigo_postal());
                
                stmt.setString(10, anunciante.getImagenes().getImagen());
                
                stmt.setInt(11, anunciante.getVendedor().getIdVendedor());
                
                stmt.setInt(12, anunciante.getFiltroAnunciante().getFiltros().getIdFiltro());

                resp=true;
                stmt.execute();
            }
            conexion.cerrar();
        } catch (Exception e) {
            e.printStackTrace();
            resp=false;
        }
        return resp;     
    }
    
    public ArrayList<Anunciante> selectAll() throws Exception {
        String sql = "select * from vistaAnunciante;";
        ConexionMySQL c = new ConexionMySQL();
        ArrayList<Anunciante> anunciante = new ArrayList<>();
        try {
            c.abrir();
            Connection conn = c.getConexion();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Persona objP =new Persona();
                objP.setIdPersona(rs.getInt(2));
                objP.setNombre(rs.getString(3));
                objP.setApellidoPaterno(rs.getString(4));
                objP.setApellidoMaterno(rs.getString(5));
                objP.setCalle(rs.getString(6));
                objP.setColonia(rs.getString(7));
                objP.setNumero(rs.getString(8));
                objP.setCodigo_postal(rs.getInt(9));
                objP.setFoto(rs.getString(10));
                objP.setFechaNacimiento(rs.getString(11));
                objP.setFechaInicio(rs.getString(12));
                objP.setGenero(rs.getInt(13));
                objP.setTelefono(rs.getString(14));
                UsuarioLogin objU =new UsuarioLogin();
                objU.setUsuario(rs.getString(15));
                objU.setContrasenia(rs.getString(16));
                objU.setRol(rs.getString(17));
                objU.setEstatus(rs.getInt(18));
                Zona objZ=new Zona();
                objZ.setIdZonas(rs.getInt(19));
                objZ.setNombre_zona(rs.getString(20));
                Vendedor objV=new Vendedor();
                objV.setIdVendedor(rs.getInt(21));
                objV.setPersona(objP);
                objV.setUsuario(objU);
                objV.setZonas(objZ);
                Anunciante objA = new Anunciante();
                objA.setVendedor(objV);
                objA.setIdAnunciante(rs.getInt(1));
                objA.setNombre_comercial(rs.getString(22));
                objA.setLogo(rs.getString(23));
                objA.setMas_imagenes(rs.getInt(24));
                objA.setDescripcion(rs.getString(25));
                objA.setCotizacion(rs.getInt(26));
                objA.setCalle(rs.getString(27));
                objA.setColonia(rs.getString(28));
                objA.setNumero(rs.getString(29));
                objA.setCodigo_postal(rs.getInt(30));
                objA.setEstatus(rs.getInt(31));
                Imagenes ima = new Imagenes();
                ima.setImagen(rs.getString(32));
                objA.setImagenes(ima);
                anunciante.add(objA);
            }   
            rs.close();
            stmt.close();
            c.cerrar();
        }catch (Exception e) {
            System.out.println(e.toString());
        }
        return anunciante;
    }
    
    public boolean deleteAnunciante(int idAnunciante){
        ConexionMySQL conexion = new ConexionMySQL();
        boolean resp= true; 
        try {
            Connection c = conexion.abrir();
            String sql = " call eliminarAnunciante(?)";
            java.sql.CallableStatement stmt = (java.sql.CallableStatement) c.prepareCall(sql);
            stmt.setInt(1, idAnunciante);
            stmt.execute();
            stmt.close();
            conexion.cerrar();
        } catch (Exception e) {
            e.printStackTrace(); 
            resp=false;

        }
        return  resp;
    }
    
    public boolean updateAnunciante (Anunciante anunciante) {
        ConexionMySQL conexion = new ConexionMySQL();
         boolean resp= false; 
        try {
            Connection c = conexion.abrir();
            String sql = " call updateAnunciantes(?,?,?,?,?,?,?,?,?,?);";
            try (CallableStatement stmt = (CallableStatement) c.prepareCall(sql)) {
                stmt.setString(1, anunciante.getNombre_comercial());
                stmt.setString(2, anunciante.getLogo());
                stmt.setInt(3, anunciante.getMas_imagenes());
                stmt.setString(4, anunciante.getDescripcion());
                stmt.setInt(5, anunciante.getCotizacion());
                stmt.setString(6, anunciante.getCalle());
                stmt.setString(7, anunciante.getColonia());
                stmt.setString(8, anunciante.getNumero());
                stmt.setInt(9, anunciante.getCodigo_postal());
                stmt.setInt(10, anunciante.getIdAnunciante());
                resp=true;
                stmt.execute();
            }
            conexion.cerrar();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return resp;
    }
}
