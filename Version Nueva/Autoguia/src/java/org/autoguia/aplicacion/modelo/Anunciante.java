/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.modelo;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 20/09/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 22/09/2020 modelo
 */
public class Anunciante {

    private int idAnunciante;
    private Vendedor vendedor;
    private String nombre_comercial;
    private String logo;
    private int mas_imagenes;
    private String descripcion;
    private int cotizacion;
    private String calle;
    private String colonia;
    private String numero;
    private int codigo_postal;
    private int estatus;
    private Imagenes imagenes;
    //private Horario horario;
    private FiltroAnunciante filtroAnunciante;

    public int getIdAnunciante() {
        return idAnunciante;
    }

    public void setIdAnunciante(int idAnunciante) {
        this.idAnunciante = idAnunciante;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public String getNombre_comercial() {
        return nombre_comercial;
    }

    public void setNombre_comercial(String nombre_comercial) {
        this.nombre_comercial = nombre_comercial;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getMas_imagenes() {
        return mas_imagenes;
    }

    public void setMas_imagenes(int mas_imagenes) {
        this.mas_imagenes = mas_imagenes;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCotizacion() {
        return cotizacion;
    }

    public void setCotizacion(int cotizacion) {
        this.cotizacion = cotizacion;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getCodigo_postal() {
        return codigo_postal;
    }

    public void setCodigo_postal(int codigo_postal) {
        this.codigo_postal = codigo_postal;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public Imagenes getImagenes() {
        return imagenes;
    }

    public void setImagenes(Imagenes imagenes) {
        this.imagenes = imagenes;
    }

    public FiltroAnunciante getFiltroAnunciante() {
        return filtroAnunciante;
    }

    public void setFiltroAnunciante(FiltroAnunciante filtroAnunciante) {
        this.filtroAnunciante = filtroAnunciante;
    }

    public Anunciante(int idAnunciante, Vendedor vendedor, String nombre_comercial, String logo, int mas_imagenes, String descripcion, int cotizacion, String calle, String colonia, String numero, int codigo_postal, int estatus, Imagenes imagenes, FiltroAnunciante filtroAnunciante) {
        this.idAnunciante = idAnunciante;
        this.vendedor = vendedor;
        this.nombre_comercial = nombre_comercial;
        this.logo = logo;
        this.mas_imagenes = mas_imagenes;
        this.descripcion = descripcion;
        this.cotizacion = cotizacion;
        this.calle = calle;
        this.colonia = colonia;
        this.numero = numero;
        this.codigo_postal = codigo_postal;
        this.estatus = estatus;
        this.imagenes = imagenes;
        this.filtroAnunciante = filtroAnunciante;
    }

    public Anunciante() {
    }

    @Override
    public String toString() {
        return "Anunciante{" + "idAnunciante=" + idAnunciante + ", vendedor=" + vendedor + ", nombre_comercial=" + nombre_comercial + ", logo=" + logo + ", mas_imagenes=" + mas_imagenes + ", descripcion=" + descripcion + ", cotizacion=" + cotizacion + ", calle=" + calle + ", colonia=" + colonia + ", numero=" + numero + ", codigo_postal=" + codigo_postal + ", estatus=" + estatus + ", imagenes=" + imagenes + ", filtroAnunciante=" + filtroAnunciante + '}';
    }
    
}
