/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.modelo;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 20/09/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 22/09/2020 modelo
 */
public class Vendedor {
    
    private int idVendedor;
    private Gerente gerente;
    private UsuarioLogin usuario;
    private Zona zonas;
    private Persona persona;

    public int getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(int idVendedor) {
        this.idVendedor = idVendedor;
    }

    public Gerente getGerente() {
        return gerente;
    }

    public void setGerente(Gerente gerente) {
        this.gerente = gerente;
    }

    public UsuarioLogin getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioLogin usuario) {
        this.usuario = usuario;
    }

    public Zona getZonas() {
        return zonas;
    }

    public void setZonas(Zona zonas) {
        this.zonas = zonas;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Vendedor() {
    }

    public Vendedor(int idVendedor, Gerente gerente, UsuarioLogin usuario, Zona zonas, Persona persona) {
        this.idVendedor = idVendedor;
        this.gerente = gerente;
        this.usuario = usuario;
        this.zonas = zonas;
        this.persona = persona;
    }
    
    public Vendedor(Persona persona, UsuarioLogin usuario, Zona zonas) {
        this.usuario = usuario;
        this.zonas = zonas;
        this.persona = persona;
    }

    @Override
    public String toString() {
        return "Vendedor{" + "idVendedor=" + idVendedor + ", gerente=" + gerente + ", usuario=" + usuario + ", zonas=" + zonas + ", persona=" + persona + '}';
    }
    
    
}
