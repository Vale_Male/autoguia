/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.modelo;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 20/09/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 22/09/2020 modelo
 */
public class UsuarioLogin {
    private int idUsuario;
    private String usuario;
    private String contrasenia;
    private String token;
    private int estatus;
    private String rol;

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public UsuarioLogin() {
    }

    public UsuarioLogin(int idUsuario, String usuario, String contrasenia, String token, int estatus, String rol) {
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.contrasenia = contrasenia;
        this.token = token;
        this.estatus = estatus;
        this.rol = rol;
    }
    
    public UsuarioLogin( String usuario, String contrasenia, String token, int estatus, String rol) {
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.contrasenia = contrasenia;
        this.token = token;
        this.estatus = estatus;
        this.rol = rol;
    }

    @Override
    public String toString() {
        return "UsuarioLogin{" + "idUsuario=" + idUsuario + ", usuario=" + usuario + ", contrasenia=" + contrasenia + ", token=" + token + ", estatus=" + estatus + ", rol=" + rol + '}';
    }
    
    
}
