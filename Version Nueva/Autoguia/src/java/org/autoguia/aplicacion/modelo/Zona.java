/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.modelo;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 20/09/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 22/09/2020 modelo
 */
public class Zona {
    
    private int idZonas;
    private Ciudad ciudad;
    private String nombre_zona;

    public int getIdZonas() {
        return idZonas;
    }

    public void setIdZonas(int idZonas) {
        this.idZonas = idZonas;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public String getNombre_zona() {
        return nombre_zona;
    }

    public void setNombre_zona(String nombre_zona) {
        this.nombre_zona = nombre_zona;
    }

    public Zona() {
    }

    public Zona(int idZonas, Ciudad ciudad, String nombre_zona) {
        this.idZonas = idZonas;
        this.ciudad = ciudad;
        this.nombre_zona = nombre_zona;
    }
    
    public Zona( Ciudad ciudad, String nombre_zona) {
        this.ciudad = ciudad;
        this.nombre_zona = nombre_zona;
    }

    @Override
    public String toString() {
        return "Zona{" + "idZonas=" + idZonas + ", ciudad=" + ciudad + ", nombre_zona=" + nombre_zona + '}';
    }
    
    
}
