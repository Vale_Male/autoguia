                            /*Módulo del Gerente*/
var gerentesAct = null;
var gerentesIna = null;
var gerente = null;

function cargarListas(){
    //alert("Si entra");
    mostrarGerentesAct();
    mostrarGerentesIna();
    mostrarVendedoresAct();
    mostrarVendedoresIna();
    mostrarPromocionesAct();
    mostrarAnunciantesAct();
    cargarAnunciante();
}

function guardarGerente() {
    var nombre = $('#txtNombre').val();
    //alert(nombre);
    var apellidoPaterno = $('#txtApellidoPaterno').val();
    //alert(apellidoPaterno);
    var apellidoMaterno = $('#txtApellidoMaterno').val();
    //alert(apellidoMaterno);
    var calle = $('#txtCalle').val();
    //alert(apellidoMaterno);
    var colonia = $('#txtColonia').val();
    //alert(apellidoMaterno);
    var numero = $('#txtNumero').val();
    //alert(apellidoMaterno);
    var codigo_postal = $('#txtCodigo_postal').val();
    //alert(apellidoMaterno);
    var foto = $('#txtFoto').val();
    //alert(apellidoMaterno);
    var fechaNacimiento = $('#txtFechaNacimiento').val();
    //alert(fechaNac);
    var fechaInicio = $('#txtFechaInicio').val();
    //alert(fechaNac);
    var genero = $('#generoPer').val();
    //alert(genero);
    var telefono = $('#txtTelefono').val();
    //alert(telefono );
    var usuario = $('#txtUsuario').val();
    
    var contrasenia = $('#txtContrasenia').val();
    
    //var token = $('#txtToken').val();
    
    var rol = $('#txtRol').val();
    
    var nombre_ciudad = $('#txtCiudad').val();
    
    var estado = $('#txtEstado').val();
    
    var datos = {
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,
        calle: calle,
        colonia: colonia,
        numero: numero,
        codigo_postal: codigo_postal,
        foto: foto,
        fechaNacimiento: fechaNacimiento,
        fechaInicio: fechaInicio,
        genero: genero,
        telefono: telefono,
        usuario: usuario,
        contrasenia: contrasenia,
        //token: token,
        rol: rol,
        nombre_ciudad: nombre_ciudad,
        estado: estado
    };
    //alert(JSON.stringify(datos));
    $.ajax({
        type: "POST",
        async: true,
        url: "api/Gerente/insert",
        data: datos
    }
    ).done(function (data) {
        if (data.result != null) {
            //alert("¡Empleado agregado con exito!");
            swal("Good job!", "¡Gerente agregado con exito!", "success");
        } else {
            alert("No se pudo agregar el Gerente");
        }
        mostrarGerentesAct();
    }
    );
}

function mostrarGerentesAct() {
    $.ajax(
            {
                type: "GET",
                url: "api/Gerente/getAll",
                async: true
            }).done(
            function (data) {
                //alert(JSON.stringify(data));
                gerentesAct = data;
                var tabla = "<table class='table table-sm table-hover table-dark' id=tablaGerentesAct> \n\
                  <caption>Gerentes Activos</caption> ";
                tabla += "<tr class='bg-info'> <th scope=col>Nombre</th> <th scope=col>Apellidos</th> <th scope=col>Calle</th> <th scope=col>Colonia</th>\n\
                 <th scope=col>Numero</th> <th scope=col>Código Postal</th> <th scope=col>Foto</th> <th scope=col>Fecha de Nacimiento</th>\n\
                 <th scope=col>Fecha de Inicio</th> <th scope=col>Género</th> <th scope=col>Teléfono</th> <th scope=col>Usuario</th>\n\
                 <th scope=col>Rol</th> <th scope=col>Ciudad</th> <th scope=col>Estado</th> <th></th> </tr>";
                for (var i = 0; i < data.length; i++) {
                    tabla += "<tr scope=row>";
                    //alert(data[i].persona.nombre);
                    tabla += "<td>" + data[i].persona.nombre + "</td>";
                    tabla += "<td>" + data[i].persona.apellidoPaterno +" "+ data[i].persona.apellidoMaterno + "</td>";
                    tabla += "<td>" + data[i].persona.calle + "</td>";
                    tabla += "<td>" + data[i].persona.colonia + "</td>";
                    tabla += "<td>" + data[i].persona.numero + "</td>";
                    tabla += "<td>" + data[i].persona.codigo_postal + "</td>";
                    tabla += "<td> <img width='42' height='42' src='" + data[i].persona.foto + "' alt='Sin-Foto ' /> </td>";
                    tabla += "<td>" + data[i].persona.fechaNacimiento + "</td>";
                    tabla += "<td>" + data[i].persona.fechaInicio + "</td>";
                    if (data[i].persona.genero == '2') {
                        tabla += "<td> Mujer </td>";
                    } else {
                        if (data[i].persona.genero == '1') {
                            tabla += "<td> Hombre </td>";
                        } else {
                            tabla += "<td> Otro </td>";
                        }
                    }
                    tabla += "<td>" + data[i].persona.telefono + "</td>";
                    tabla += "<td>" + data[i].usuario.usuario + "</td>";
                    //tabla += "<td>" + data[i].usuario.contrasenia + "</td>";
                    //tabla += "<td>" + data[i].usuario.token + "</td>";
                    tabla += "<td>" + data[i].usuario.rol + "</td>";
                    tabla += "<td>" + data[i].ciudad.nombre_ciudad + "</td>";
                    tabla += "<td>" + data[i].ciudad.estado + "</td>";
                    
                    tabla += "<td> <button class='btn btn-outline-info' onclick='cargarModificarGerente( " + i + ") '>Modificar</button></td>";
                    tabla += "<td> <button class='btn btn-outline-danger' onclick='eliminarGerente( " + i + ") '>Eliminar</button></td>";
                    tabla += "</tr>";

                }
                                   
                tabla += "</table>";
                //alert(tabla);
                $('#tablaGerentesActi').html(tabla);
            });
}

function mostrarGerentesIna() {
    $.ajax(
            {
                type: "GET",
                url: "api/Gerente/getAllDelete",
                async: true
            }).done(
            function (data) {
                //alert(JSON.stringify(data));
                gerentesIna = data;
                var tabla = "<table class='table table-sm table-hover table-dark' id=tablaGerentesIna> \n\
                  <caption>Gerentes Inactivos</caption> ";
                tabla += "<tr class='bg-info'> <th scope=col>Nombre</th> <th scope=col>Apellidos</th> <th scope=col>Calle</th> <th scope=col>Colonia</th>\n\
                 <th scope=col>Numero</th> <th scope=col>Código Postal</th> <th scope=col>Foto</th> <th scope=col>Fecha de Nacimiento</th>\n\
                 <th scope=col>Fecha de Inicio</th> <th scope=col>Género</th> <th scope=col>Teléfono</th> <th scope=col>Usuario</th>\n\
                 <th scope=col>Rol</th> <th scope=col>Ciudad</th> <th scope=col>Estado</th> <th></th> </tr>";
                for (var i = 0; i < data.length; i++) {
                    tabla += "<tr scope=row>";
                    //alert(data[i].persona.nombre);
                    tabla += "<td>" + data[i].persona.nombre + "</td>";
                    tabla += "<td>" + data[i].persona.apellidoPaterno +" "+ data[i].persona.apellidoMaterno + "</td>";
                    tabla += "<td>" + data[i].persona.calle + "</td>";
                    tabla += "<td>" + data[i].persona.colonia + "</td>";
                    tabla += "<td>" + data[i].persona.numero + "</td>";
                    tabla += "<td>" + data[i].persona.codigo_postal + "</td>";
                    tabla += "<td> <img width='42' height='42' src='" + data[i].persona.foto + "' alt='Sin-Foto ' /> </td>";
                    tabla += "<td>" + data[i].persona.fechaNacimiento + "</td>";
                    tabla += "<td>" + data[i].persona.fechaInicio + "</td>";
                    if (data[i].persona.genero == '2') {
                        tabla += "<td> Mujer </td>";
                    } else {
                        if (data[i].persona.genero == '1') {
                            tabla += "<td> Hombre </td>";
                        } else {
                            tabla += "<td> Otro </td>";
                        }
                    }
                    tabla += "<td>" + data[i].persona.telefono + "</td>";
                    tabla += "<td>" + data[i].usuario.usuario + "</td>";
                    //tabla += "<td>" + data[i].usuario.contrasenia + "</td>";
                    //tabla += "<td>" + data[i].usuario.token + "</td>";
                    tabla += "<td>" + data[i].usuario.rol + "</td>";
                    tabla += "<td>" + data[i].ciudad.nombre_ciudad + "</td>";
                    tabla += "<td>" + data[i].ciudad.estado + "</td>";
                    
                    tabla += "<td> <button class='btn btn-outline-success' onclick='ActivarGerente( " + i + ") '>Reactivar</button></td>";
                    tabla += "</tr>";

                }
                                   
                tabla += "</table>";
                //alert(tabla);
                $('#tablaGerentesInac').html(tabla);
            });
}

function eliminarGerente(i) {
    var mm= gerentesAct[i].idGerente;
    var data={idGerente:mm};
    $.ajax({
        type: "POST",
        async: true,
        url: "api/Gerente/delete",
        data:data
    }
    ).done(function (data) {
        if (data.result != null){
        //    alert("No se pudo hacer el delate");
        }else{
        //   alert("si pudo ");
 }
        //alert("¡Eliminación exitosa del Empleado!");
        swal("Good job!", "¡Eliminación exitosa del Gerente!", "success");
        cargarModuloGerente();
    }
    );
}

function cargarModuloGerente() {
    $.ajax({
        type: "GET",
        url: "gerente.html",
        async: true
    }
    ).done(
            function (data)
            {
                mostrarGerentesAct();
                mostrarGerentesIna();
            }
    );
}

function ActivarGerente(i) {
    var mm = gerentesIna[i].idGerente;
    var data = {idGerente: mm};
    $.ajax({
        type: "POST",
        async: true,
        url: "api/Gerente/reactivate",
        data: data
    }
    ).done(function (data) {
        if (data.result != null) {
            //    alert("No se pudo hacer el delate");
        } else {
            //   alert("si pudo ");
        }

        // swal("¡Se ha reactivado correctamente el Empleado!");
        swal("Good job!", "¡Se ha reactivado correctamente el Gerente!", "success");

        cargarModuloGerente();
    }
    );
}

function buscarGerente() {
    var tableReg = document.getElementById('tablaGerentesAct');
    var gen = $('#searchGerente').val();
    var searchText = document.getElementById('searchGerente').value.toLowerCase();

    for (var i = 1; i < tableReg.rows.length; i++) {
        var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');

        var found = false;
        for (var j = 0; j < ((cellsOfRow.length) - 3) && !found; j++) {
            var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }

    var tableRegp = document.getElementById('tablaGerentesIna');
    var genp = $('#searchGerente').val();
   // var searchTextp = document.getElementById('searchEmpleado').value.toLowerCase();    
    for (var ip = 1; ip < tableRegp.rows.length; ip++) {
        var cellsOfRowp = tableRegp.rows[ip].getElementsByTagName('td');
        var foundp = false;
        for (var jp = 0; jp < ((cellsOfRowp.length) - 2) && !foundp; jp++) {
            var compareWithp = cellsOfRowp[jp].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWithp.indexOf(searchText) > -1)) {
                foundp = true;
            }
        }
        if (foundp) {
            tableRegp.rows[ip].style.display = '';
        } else {
            tableRegp.rows[ip].style.display = 'none';
        }
    }
}

function guardarModificarGerente() {  
    var idGerente=$('#txtIdGerente').val();
    
    var nombre = $('#txtNombre').val();
    //alert(nombre);
    var apellidoPaterno = $('#txtApellidoPaterno').val();
    //alert(apellidoPaterno);
    var apellidoMaterno = $('#txtApellidoMaterno').val();
    //alert(apellidoMaterno);
    var calle = $('#txtCalle').val();
    //alert(apellidoMaterno);
    var colonia = $('#txtColonia').val();
    //alert(apellidoMaterno);
    var numero = $('#txtNumero').val();
    //alert(apellidoMaterno);
    var codigo_postal = $('#txtCodigo_postal').val();
    //alert(apellidoMaterno);
    var foto = $('#txtFoto').val();
    //alert(apellidoMaterno);
    var fechaNacimiento = $('#txtFechaNacimiento').val();
    //alert(fechaNac);
    var fechaInicio = $('#txtFechaInicio').val();
    //alert(fechaNac);
    var genero = $('#generoPer').val();
    //alert(genero);
    var telefono = $('#txtTelefono').val();
    //alert(telefono );
    var info = {
        idGerente: idGerente,
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,
        calle: calle,
        colonia: colonia,
        numero: numero,
        codigo_postal: codigo_postal,
        foto: foto,
        fechaNacimiento: fechaNacimiento,
        fechaInicio: fechaInicio,
        genero: genero,
        telefono: telefono
    };
    $.ajax({
        type: "POST",
        async: true,
        url: "api/Gerente/update",
        data: info
    }
    ).done(function (data) {
        if (data.result != null){
           
            //alert("¡Actualización exitosa del Empleado!");
            swal("Good job!", "¡Actualización exitosa del Gerente!", "success");
        }else{
            alert("¡No se puedo actualizar el Gerente!");}
        mostrarGerentesAct();
    }
    );
}

function cargarModificarGerente(posicion) {
   
    gerente = gerentesAct[posicion]; 
    $.ajax({
        type: "GET",
        url: "gerente.html",
        async: true
    }
    ).done(
            function (data)
            {                
                $('#txtIdGerente').val(gerente.idGerente);
                
                $('#txtNombre').val(gerente.persona.nombre);
                $('#txtApellidoPaterno').val(gerente.persona.apellidoPaterno);
                $('#txtApellidoMaterno').val(gerente.persona.apellidoMaterno);
                $('#txtCalle').val(gerente.persona.calle);                
                $('#txtColonia').val(gerente.persona.colonia);
                $('#txtNumero').val(gerente.persona.numero);                
                $('#txtCodigo_postal').val(gerente.persona.codigo_postal);                
                $('#txtFoto').val(gerente.persona.foto);                
                $('#txtFechaNacimiento').val(gerente.persona.fechaNacimiento);               
                $('#txtFechaInicio').val(gerente.persona.fechaInicio);
                $('#txtTelefono').val(gerente.persona.telefono);
                
                if(gerente.persona.genero =="2"){
                    document.getElementById("generoPer").selectedIndex = "1";
                }else{
                    if(gerente.persona.genero =="1"){
                        document.getElementById("generoPer").selectedIndex = "0";
                    }else{
                        document.getElementById("generoPer").selectedIndex = "2";
                    }
                }                                    
            }
    );
}

function decidirGerente(){
    if (gerente== null){
        guardarGerente();
    }else{
        guardarModificarGerente();      
    }
    limpiarCampos();
}

function encodeImagetoBase64(element) {
    var file = element.files[0];
    var reader = new FileReader();
    var base64 = "";
    
    reader.onloadend = function () {
        base64 = reader.result;
        $('#txtFoto').val(base64);
        $('#Imgfoto').attr("src", base64);
        $("#imgSala").show();
    };
    reader.readAsDataURL(file);
}

function limpiarCampos(){
    $('#txtNombre').val("");
    $('#txtApellidoPaterno').val("");
    $('#txtApellidoMaterno').val("");
    $('#txtCalle').val("");
    $('#txtColonia').val("");
    $('#txtNumero').val("");
    $('#txtCodigo_postal').val("");
    $('#txtFechaNacimiento').val("");
    $('#txtFechaInicio').val("");
    $('#generoPer').val("");
    $('#txtTelefono').val("");
}


/*-------------------------------------Módulo del Vendedor---------------------------*/
var vendedoresAct = null;
var vendedoresIna = null;
var vendedor = null;

function guardarVendedor() {
    var nombre = $('#txtNombre').val();
    //alert(nombre);
    var apellidoPaterno = $('#txtApellidoPaterno').val();
    //alert(apellidoPaterno);
    var apellidoMaterno = $('#txtApellidoMaterno').val();
    //alert(apellidoMaterno);
    var calle = $('#txtCalle').val();
    //alert(apellidoMaterno);
    var colonia = $('#txtColonia').val();
    //alert(apellidoMaterno);
    var numero = $('#txtNumero').val();
    //alert(apellidoMaterno);
    var codigo_postal = $('#txtCodigo_postal').val();
    //alert(apellidoMaterno);
    var foto = $('#txtFoto').val();
    //alert(apellidoMaterno);
    var fechaNacimiento = $('#txtFechaNacimiento').val();
    //alert(fechaNac);
    var fechaInicio = $('#txtFechaInicio').val();
    //alert(fechaNac);
    var genero = $('#generoPer').val();
    //alert(genero);
    var telefono = $('#txtTelefono').val();
    //alert(telefono );
    var usuario = $('#txtUsuario').val();
    
    var contrasenia = $('#txtContrasenia').val();
        
    var status = $('#txtEstatus').val();
    
    var rol = $('#txtRol').val();
    
    
    var nombre_ciudad = $('#txtCiudad').val();
    
    var estado = $('#txtEstado').val();
    
    var zona = $('#txtZona').val();
    
    var datos = {
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,
        calle: calle,
        colonia: colonia,
        numero: numero,
        codigo_postal: codigo_postal,
        foto: foto,
        fechaNacimiento: fechaNacimiento,
        fechaInicio: fechaInicio,
        genero: genero,
        telefono: telefono,
        usuario: usuario,
        contrasenia: contrasenia,
        status: status,
        rol: rol,
        nombre_ciudad: nombre_ciudad,
        estado: estado,
        zona: zona
    };
    //alert(JSON.stringify(datos));
    $.ajax({
        type: "POST",
        async: true,
        url: "api/vendedor/insert",
        data: datos
    }
    ).done(function (data) {
        if (data.result != null) {
            //alert("¡Empleado agregado con exito!");
            swal("Good job!", "¡Vendedor agregado con exito!", "success");
        } else {
            alert("No se pudo agregar el Vendedor");
        }
        mostrarVendedoresAct()();
    }
    );
}

function mostrarVendedoresAct() {
    $.ajax(
            {
                type: "GET",
                url: "api/vendedor/getAll",
                async: true
            }).done(
            function (data) {
                //alert(JSON.stringify(data));
                vendedoresAct = data;
                var tabla = "<table class='table table-sm table-hover table-dark' id=tablaVendedoresAct> \n\
                  <caption>Vendedores Activos</caption> ";
                tabla += "<tr class='bg-info'> <th scope=col>Nombre</th> <th scope=col>Apellidos</th> <th scope=col>Calle</th> <th scope=col>Colonia</th>\n\
                 <th scope=col>Numero</th> <th scope=col>Código Postal</th> <th scope=col>Foto</th> <th scope=col>Fecha de Nacimiento</th>\n\
                 <th scope=col>Fecha de Inicio</th> <th scope=col>Género</th> <th scope=col>Teléfono</th> <th scope=col>Usuario</th>\n\
                 <th scope=col>Rol</th> <th scope=col>Zona</th> <th></th> </tr>";
                for (var i = 0; i < data.length; i++) {
                    tabla += "<tr scope=row>";
                    //alert(data[i].persona.nombre);
                    tabla += "<td>" + data[i].persona.nombre + "</td>";
                    tabla += "<td>" + data[i].persona.apellidoPaterno +" "+ data[i].persona.apellidoMaterno + "</td>";
                    tabla += "<td>" + data[i].persona.calle + "</td>";
                    tabla += "<td>" + data[i].persona.colonia + "</td>";
                    tabla += "<td>" + data[i].persona.numero + "</td>";
                    tabla += "<td>" + data[i].persona.codigo_postal + "</td>";
                    tabla += "<td> <img width='42' height='42' src='" + data[i].persona.foto + "' alt='Sin-Foto ' /> </td>";
                    tabla += "<td>" + data[i].persona.fechaNacimiento + "</td>";
                    tabla += "<td>" + data[i].persona.fechaInicio + "</td>";
                    if (data[i].persona.genero == '2') {
                        tabla += "<td> Mujer </td>";
                    } else {
                        if (data[i].persona.genero == '1') {
                            tabla += "<td> Hombre </td>";
                        } else {
                            tabla += "<td> Otro </td>";
                        }
                    }
                    tabla += "<td>" + data[i].persona.telefono + "</td>";
                    tabla += "<td>" + data[i].usuario.usuario + "</td>";
                    //tabla += "<td>" + data[i].usuario.contrasenia + "</td>";
                    //tabla += "<td>" + data[i].usuario.token + "</td>";
                    tabla += "<td>" + data[i].usuario.rol + "</td>";
                    
                    tabla += "<td>" + data[i].zonas.nombre_zona + "</td>";
                    
                    tabla += "<td> <button class='btn btn-outline-info' onclick='cargarModificarVendedor( " + i + ") '>Modificar</button></td>";
                    tabla += "<td> <button class='btn btn-outline-danger' onclick='eliminarVendedor( " + i + ") '>Eliminar</button></td>";
                    tabla += "</tr>";

                }
                                   
                tabla += "</table>";
                //alert(tabla);
                $('#tablaVendedoresActi').html(tabla);
            });
}

function mostrarVendedoresIna() {
    $.ajax(
            {
                type: "GET",
                url: "api/vendedor/getAllDelete",
                async: true
            }).done(
            function (data) {
                //alert(JSON.stringify(data));
                vendedoresIna = data;
                var tabla = "<table class='table table-sm table-hover table-dark' id=tablaVendedoresIna> \n\
                  <caption>Vendedores Activos</caption> ";
                tabla += "<tr class='bg-info'> <th scope=col>Nombre</th> <th scope=col>Apellidos</th> <th scope=col>Calle</th> <th scope=col>Colonia</th>\n\
                 <th scope=col>Numero</th> <th scope=col>Código Postal</th> <th scope=col>Foto</th> <th scope=col>Fecha de Nacimiento</th>\n\
                 <th scope=col>Fecha de Inicio</th> <th scope=col>Género</th> <th scope=col>Teléfono</th> <th scope=col>Usuario</th>\n\
                 <th scope=col>Rol</th> <th scope=col>Zona</th> <th></th> </tr>";
                for (var i = 0; i < data.length; i++) {
                    tabla += "<tr scope=row>";
                    //alert(data[i].persona.nombre);
                    tabla += "<td>" + data[i].persona.nombre + "</td>";
                    tabla += "<td>" + data[i].persona.apellidoPaterno +" "+ data[i].persona.apellidoMaterno + "</td>";
                    tabla += "<td>" + data[i].persona.calle + "</td>";
                    tabla += "<td>" + data[i].persona.colonia + "</td>";
                    tabla += "<td>" + data[i].persona.numero + "</td>";
                    tabla += "<td>" + data[i].persona.codigo_postal + "</td>";
                    tabla += "<td> <img width='42' height='42' src='" + data[i].persona.foto + "' alt='Sin-Foto ' /> </td>";
                    tabla += "<td>" + data[i].persona.fechaNacimiento + "</td>";
                    tabla += "<td>" + data[i].persona.fechaInicio + "</td>";
                    if (data[i].persona.genero == '2') {
                        tabla += "<td> Mujer </td>";
                    } else {
                        if (data[i].persona.genero == '1') {
                            tabla += "<td> Hombre </td>";
                        } else {
                            tabla += "<td> Otro </td>";
                        }
                    }
                    tabla += "<td>" + data[i].persona.telefono + "</td>";
                    tabla += "<td>" + data[i].usuario.usuario + "</td>";
                    tabla += "<td>" + data[i].usuario.rol + "</td>";
                    
                    tabla += "<td>" + data[i].zonas.nombre_zona + "</td>";
                    
                    tabla += "<td> <button class='btn btn-outline-success' onclick='ActivarVendedor( " + i + ") '>Reactivar</button></td>";
                    tabla += "</tr>";

                }
                                   
                tabla += "</table>";
                //alert(tabla);
                $('#tablaVendedoresInac').html(tabla);
            });
}

function eliminarVendedor(i) {
    var mn= vendedoresAct[i].idVendedor;
    var data={idVendedor:mn};
    $.ajax({
        type: "POST",
        async: true,
        url: "api/vendedor/delete",
        data:data
    }
    ).done(function (data) {
        if (data.result != null){
        //    alert("No se pudo hacer el delate");
        }else{
        //   alert("si pudo ");
 }
        //alert("¡Eliminación exitosa del Empleado!");
        swal("Good job!", "¡Eliminación exitosa del Vendedor!", "success");
        cargarModuloVendedor();
    }
    );
}

function cargarModuloVendedor() {
    $.ajax({
        type: "GET",
        url: "vendedor.html",
        async: true
    }
    ).done(
            function (data)
            {
                mostrarVendedoresAct();
                mostrarVendedoresIna();
            }
    );
}

function ActivarVendedor(i) {
    var mn = vendedoresIna[i].idVendedor;
    var data = {idVendedor: mn};
    $.ajax({
        type: "POST",
        async: true,
        url: "api/vendedor/active",
        data: data
    }
    ).done(function (data) {
        if (data.result != null) {
            //    alert("No se pudo hacer el delate");
        } else {
            //   alert("si pudo ");
        }

        // swal("¡Se ha reactivado correctamente el Empleado!");
        swal("Good job!", "¡Se ha reactivado correctamente el Vendedor!", "success");
        cargarModuloVendedor();
    }
    );
}

function buscarVendedor() {
    var tableReg = document.getElementById('tablaVendedoresAct');
    var gen = $('#searchVendedor').val();
    var searchText = document.getElementById('searchVendedor').value.toLowerCase();

    for (var i = 1; i < tableReg.rows.length; i++) {
        var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');

        var found = false;
        for (var j = 0; j < ((cellsOfRow.length) - 3) && !found; j++) {
            var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }

    var tableRegp = document.getElementById('tablaVendedoresIna');
    var genp = $('#searchVendedor').val();
   // var searchTextp = document.getElementById('searchEmpleado').value.toLowerCase();    
    for (var ip = 1; ip < tableRegp.rows.length; ip++) {
        var cellsOfRowp = tableRegp.rows[ip].getElementsByTagName('td');
        var foundp = false;
        for (var jp = 0; jp < ((cellsOfRowp.length) - 2) && !foundp; jp++) {
            var compareWithp = cellsOfRowp[jp].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWithp.indexOf(searchText) > -1)) {
                foundp = true;
            }
        }
        if (foundp) {
            tableRegp.rows[ip].style.display = '';
        } else {
            tableRegp.rows[ip].style.display = 'none';
        }
    }
}

function guardarModificarVendedor() {  
    var idPersona=$('#txtIdPersona').val();
    
    var nombre = $('#txtNombre').val();
    //alert(nombre);
    var apellidoPaterno = $('#txtApellidoPaterno').val();
    //alert(apellidoPaterno);
    var apellidoMaterno = $('#txtApellidoMaterno').val();
    //alert(apellidoMaterno);
    var calle = $('#txtCalle').val();
    //alert(apellidoMaterno);
    var colonia = $('#txtColonia').val();
    //alert(apellidoMaterno);
    var numero = $('#txtNumero').val();
    //alert(apellidoMaterno);
    var codigo_postal = $('#txtCodigo_postal').val();
    //alert(apellidoMaterno);
    var foto = $('#txtFoto').val();
    //alert(apellidoMaterno);
    var fechaNacimiento = $('#txtFechaNacimiento').val();
    //alert(fechaNac);
    var fechaInicio = $('#txtFechaInicio').val();
    //alert(fechaNac);
    var genero = $('#generoPer').val();
    //alert(genero);
    var telefono = $('#txtTelefono').val();
    //alert(telefono );
    var info = {
        idPersona: idPersona,
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,
        calle: calle,
        colonia: colonia,
        numero: numero,
        codigo_postal: codigo_postal,
        foto: foto,
        fechaNacimiento: fechaNacimiento,
        fechaInicio: fechaInicio,
        genero: genero,
        telefono: telefono
    };
    $.ajax({
        type: "POST",
        async: true,
        url: "api/vendedor/update",
        data: info
    }
    ).done(function (data) {
        if (data.result != null){
           
            //alert("¡Actualización exitosa del Empleado!");
            swal("Good job!", "¡Actualización exitosa del Vendedor!", "success");
        }else{
            alert("¡No se puedo actualizar el Vendedor!");}
        mostrarVendedoresAct()();
    }
    );
}

function cargarModificarVendedor(posicion) {
   
    vendedor = vendedoresAct[posicion]; 
    $.ajax({
        type: "GET",
        url: "vendedor.html",
        async: true
    }
    ).done(
            function (data)
            {                
                $('#txtIdPersona').val(vendedor.persona.idPersona);
                
                $('#txtNombre').val(vendedor.persona.nombre);
                $('#txtApellidoPaterno').val(vendedor.persona.apellidoPaterno);
                $('#txtApellidoMaterno').val(vendedor.persona.apellidoMaterno);
                $('#txtCalle').val(vendedor.persona.calle);                
                $('#txtColonia').val(vendedor.persona.colonia);
                $('#txtNumero').val(vendedor.persona.numero);                
                $('#txtCodigo_postal').val(vendedor.persona.codigo_postal);                
                $('#txtFoto').val(vendedor.persona.foto);                
                $('#txtFechaNacimiento').val(vendedor.persona.fechaNacimiento);               
                $('#txtFechaInicio').val(vendedor.persona.fechaInicio);
                $('#txtTelefono').val(vendedor.persona.telefono);
                
                if(vendedor.persona.genero =="2"){
                    document.getElementById("generoPer").selectedIndex = "1";
                }else{
                    if(vendedor.persona.genero =="1"){
                        document.getElementById("generoPer").selectedIndex = "0";
                    }else{
                        document.getElementById("generoPer").selectedIndex = "2";
                    }
                }                                    
            }
    );
}

function decidirVendedor(){
    if (vendedor== null){
        guardarVendedor()()();
    }else{
        guardarModificarVendedor()();      
    }
    limpiarCampos();
}           

/*-------------------------------------Módulo del Promociones---------------------------*/
var promocionesAct = null;
var promocionesIna = null;
var promocion = null;

function guardarPromocion(){
    var imagen_base = $('#txtFoto').val();
    alert(imagen_base);
    var descripcion = $('#txtDescripcion').val();
    alert(descripcion);
    var vigencia_inicio = $('#txtVigenciaInicio').val();
    alert(vigencia_inicio);
    var vigencia_fin = $('#txtVigenciaFin').val();
    alert(vigencia_fin);
    var servicio_domicilio = $('#txtServicioDomicilio').val();
    alert(servicio_domicilio);
    var anunciante = $('#lsAnunciantes').val();
    alert(anunciante);
    var datos = {
        imagen_base: imagen_base,
        descripcion: descripcion,
        vigencia_inicio: vigencia_inicio,
        vigencia_fin: vigencia_fin,
        servicio_domicilio: servicio_domicilio,
        anunciante: anunciante
    };
    $.ajax({
        type: "POST",
        async: true,
        url: "api/promociones/insert",
        data: datos
    }
    ).done(function (data) {
        if (data.result != null) {
            //alert("¡Empleado agregado con exito!");
            swal("Correcto!", "¡Promoción agregada con exito!", "success");
        } else {
            alert("No se pudo agregar la Promoción");
        }
        mostrarAnunciantesAct();
    }
    );
}

function mostrarPromocionesAct() {
    $.ajax(
            {
                type: "GET",
                url: "api/promociones/getAll",
                async: true
            }).done(
            function (data) {
                //alert(JSON.stringify(data));
                promocionesAct = data;
                var tabla = "<table class='table table-sm table-hover table-dark' id=tablaPromocionesAct> \n\
                  <caption>Promociones disponibles</caption> ";
                tabla += "<tr class='bg-info'> <th scope=col>Imagen Base</th> <th scope=col>Descripción</th> <th scope=col>Vigencia Inicio</th> <th scope=col>Vigencia Fin</th>\n\
                 <th scope=col>Servicio a domicilio</th> <th scope=col>Anunciante</th> <th>Imagen</th> </tr>";
                for (var i = 0; i < data.length; i++) {
                    tabla += "<tr scope=row>";
                    //alert(data[i].persona.nombre);
                    tabla += "<td>" + data[i].imagen_base + "</td>";
                    tabla += "<td>" + data[i].descripcion + "</td>";
                    tabla += "<td>" + data[i].vigencia_inicio + "</td>";
                    tabla += "<td>" + data[i].vigencia_fin + "</td>";
                    tabla += "<td>" + data[i].servicio_domicilio + "</td>";
                    tabla += "<td>" + data[i].anunciante.nombre_comercial + "</td>";
                    /*tabla += "<td>" + data[i].anunciante.logo + "</td>";
                    tabla += "<td>" + data[i].anunciante.mas_imagenes + "</td>";
                    tabla += "<td>" + data[i].anunciante.descripcion + "</td>";
                    tabla += "<td>" + data[i].anunciante.cotizacion + "</td>";
                    tabla += "<td>" + data[i].anunciante.calle + "</td>";
                    tabla += "<td>" + data[i].anunciante.colonia + "</td>";
                    tabla += "<td>" + data[i].anunciante.numero + "</td>";
                    tabla += "<td>" + data[i].anunciante.codigo_postal + "</td>";*/
                    tabla += "<td> <img width='42' height='42' src='" + data[i].imagen.imagen + "' alt='Sin-Foto ' /> </td>";
                    
                    tabla += "<td> <button class='btn btn-outline-info' onclick='cargarModificarPromocion( " + i + ") '>Modificar</button></td>";
                    tabla += "<td> <button class='btn btn-outline-danger' onclick='eliminarPromoción( " + i + ") '>Eliminar</button></td>";
                    tabla += "</tr>";

                }
                                   
                tabla += "</table>";
                //alert(tabla);
                $('#tablaPromocionesActi').html(tabla);
            });
}

function eliminarPromoción(i) {
    var mn= promocionesAct[i].idPromocion;
    var data={idPromocion:mn};
    $.ajax({
        type: "POST",
        async: true,
        url: "api/promociones/delete",
        data:data
    }
    ).done(function (data) {
        if (data.result != null){
        //    alert("No se pudo hacer el delate");
        }else{
        //   alert("si pudo ");
 }
        //alert("¡Eliminación exitosa del Empleado!");
        swal("Correcto!", "¡Eliminación exitosa de la promoción!", "success");
        cargarModuloPromociones();
    }
    );
}

function cargarModuloPromociones() {
    $.ajax({
        type: "GET",
        url: "promocion.html",
        async: true
    }
    ).done(
            function (data)
            {
                mostrarPromocionesAct();
            }
    );
}

function decidirPromocion(){
    if (promocion== null){
        guardarPromocion();
    }else{
        guardarModificarPromocion();      
    }
}

function guardarModificarPromocion() {  
    var idPromocion=$('#txtIdPromocion').val();
    
    var imagen_base = $('#txtFoto').val();
    //alert(imagen_base);
    var descripcion = $('#txtDescripcion').val();
    //alert(descripcion);
    var vigencia_inicio = $('#txtVigenciaInicio').val();
    //alert(vigencia_inicio);
    var vigencia_fin = $('#txtVigenciaFin').val();
    //alert(vigencia_fin);
    var servicio_domicilio = $('#txtServicioDomicilio').val();
    //alert(servicio_domicilio);
    var info = {
        idPromocion: idPromocion,
        imagen_base: imagen_base,
        descripcion: descripcion,
        vigencia_inicio: vigencia_inicio,
        vigencia_fin: vigencia_fin,
        servicio_domicilio: servicio_domicilio
    };
    $.ajax({
        type: "POST",
        async: true,
        url: "api/promociones/update",
        data: info
    }
    ).done(function (data) {
        if (data.result != null){
           
            //alert("¡Actualización exitosa del Empleado!");
            swal("Correcto!", "¡Actualización exitosa de la Promoción!", "success");
        }else{
            alert("¡No se puedo actualizar la Promoción!");}
        mostrarPromocionesAct();
    }
    );
}

function cargarModificarPromocion(posicion) {
   
    promocion = promocionesAct[posicion]; 
    $.ajax({
        type: "GET",
        url: "promocion.html",
        async: true
    }
    ).done(
            function (data)
            {                
                $('#txtIdPromocion').val(promocion.idPromocion);
                
                $('#txtFoto').val(promocion.imagen_base);
                $('#txtDescripcion').val(promocion.descripcion);
                $('#txtVigenciaInicio').val(promocion.vigencia_inicio);
                $('#txtVigenciaFin').val(promocion.vigencia_fin);                
                $('#txtServicioDomicilio').val(promocion.servicio_domicilio);                                                             
            }
    );
}

function buscarPromocion() {
    var tableReg = document.getElementById('tablaPromocionesAct');
    var gen = $('#searchPromocion').val();
    var searchText = document.getElementById('searchPromocion').value.toLowerCase();

    for (var i = 1; i < tableReg.rows.length; i++) {
        var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');

        var found = false;
        for (var j = 0; j < ((cellsOfRow.length) - 3) && !found; j++) {
            var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }
        if (foundp) {
            tableRegp.rows[ip].style.display = '';
        } else {
            tableRegp.rows[ip].style.display = 'none';
        }
}

/*-------------------------------------Módulo del Anunciante---------------------------*/
var anunciantesAct = null;
var anunciantesIna = null;
var anunciante = null;

function mostrarAnunciantesAct() {
    $.ajax(
            {
                type: "GET",
                url: "api/Anunciante/getAll",
                async: true
            }).done(
            function (data) {
                //alert(JSON.stringify(data));
                anunciantesAct = data;
                var tabla = "<table class='table table-sm table-hover table-dark' id=tablaAnunciantesAct> \n\
                  <caption>Anunciantes disponibles</caption> ";
                tabla += "<tr class='bg-info'> <th scope=col>Nombre Comercial</th> <th scope=col>Logo</th> <th scope=col>Más Imágenes</th> <th scope=col>Descripción</th>\n\
                 <th scope=col>Cotización</th> <th scope=col>Calle</th> <th scope=col>Colonia</th> <th scope=col>Número</th>\n\
                 <th scope=col>Código Postal</th> <th scope=col>Imagen</th></tr>";
                for (var i = 0; i < data.length; i++) {
                    tabla += "<tr scope=row>";
                    //alert(data[i].persona.nombre);
                    /*tabla += "<td>" + data[i].vendedor.persona.nombre + "</td>";
                    tabla += "<td>" + data[i].vendedor.persona.apellidoPaterno +" "+ data[i].vendedor.persona.apellidoMaterno + "</td>";
                    tabla += "<td>" + data[i].vendedor.persona.calle + "</td>";
                    tabla += "<td>" + data[i].vendedor.persona.colonia + "</td>";
                    tabla += "<td>" + data[i].vendedor.persona.numero + "</td>";
                    tabla += "<td>" + data[i].vendedor.persona.codigo_postal + "</td>";
                    tabla += "<td> <img width='42' height='42' src='" + data[i].vendedor.persona.foto + "' alt='Sin-Foto ' /> </td>";
                    tabla += "<td>" + data[i].vendedor.persona.fechaNacimiento + "</td>";
                    tabla += "<td>" + data[i].vendedor.persona.fechaInicio + "</td>";
                    if (data[i].vendedor.persona.genero == '2') {
                        tabla += "<td> Mujer </td>";
                    } else {
                        if (data[i].vendedor.persona.genero == '1') {
                            tabla += "<td> Hombre </td>";
                        } else {
                            tabla += "<td> Otro </td>";
                        }
                    }                                      
                    tabla += "<td>" + data[i].vendedor.persona.telefono + "</td>";
                    tabla += "<td>" + data[i].vendedor.usuario.usuario + "</td>";
                    tabla += "<td>" + data[i].vendedor.usuario.rol + "</td>";
                    
                    tabla += "<td>" + data[i].vendedor.zonas.nombre_zona + "</td>";*/
                    
                    tabla += "<td>" + data[i].nombre_comercial + "</td>";
                    tabla += "<td>" + data[i].logo + "</td>";
                    tabla += "<td>" + data[i].mas_imagenes + "</td>";
                    tabla += "<td>" + data[i].descripcion + "</td>";
                    tabla += "<td>" +"$"+ data[i].cotizacion + "</td>";
                    tabla += "<td>" + data[i].calle + "</td>";
                    tabla += "<td>" + data[i].colonia + "</td>";
                    tabla += "<td>" +"#"+ data[i].numero + "</td>";
                    tabla += "<td>" + data[i].codigo_postal + "</td>";
                    tabla += "<td> <img width='42' height='42' src='" + data[i].imagenes.imagen + "' alt='Sin-Foto ' /> </td>";
                    
                    tabla += "<td> <button class='btn btn-outline-info' onclick='cargarModificarAnunciante( " + i + ") '>Modificar</button></td>";
                    tabla += "<td> <button class='btn btn-outline-danger' onclick='eliminarAnunciante( " + i + ") '>Eliminar</button></td>";
                    tabla += "</tr>";

                }
                                   
                tabla += "</table>";
                //alert(tabla);
                $('#tablaAnunciantesActi').html(tabla);
            });
}

function cargarAnunciante() {
    $.ajax(
            {
                type: "GET",
                url: "api/Anunciante/getAll",
                async: false
            }).done(
            function (data) {
                var anunciante = "";
                for (var i = 0; i < data.length; i++) {
                    anunciante += "<option value='" + data[i].idAnunciante + "'  >"
                    anunciante += "<td>" + data[i].nombre_comercial + "</td>";
                    anunciante += "</option>";
                }
                $('#lsAnunciantes').html(anunciante);
            }
    );
}

function eliminarAnunciante(i) {
    var mn= anunciantesAct[i].idAnunciante;
    var data={idAnunciante:mn};
    $.ajax({
        type: "POST",
        async: true,
        url: "api/Anunciante/delete",
        data:data
    }
    ).done(function (data) {
        if (data.result != null){
        //    alert("No se pudo hacer el delate");
        }else{
        //   alert("si pudo ");
 }
        //alert("¡Eliminación exitosa del Empleado!");
        swal("Correcto!","¡Eliminación exitosa del Anunciante!", "success");
        cargarModuloAnunciante();
    }
    );
}

function cargarModuloAnunciante() {
    $.ajax({
        type: "GET",
        url: "anunciante.html",
        async: true
    }
    ).done(
            function (data)
            {
                mostrarAnunciantesAct();
            }
    );
}

function guardarModificarAnunciante() {  
    var idAnunciante=$('#txtIdAnunciante').val();
    
    var nombre_comercial = $('#txtNombre_comercial').val();
    //alert(imagen_base);
    var logo = $('#txtLogo').val();
    //alert(descripcion);
    var mas_imagenes = $('#txtMas_imagenes').val();
    //alert(vigencia_inicio);
    var descripcion = $('#txtDescripcion').val();
    //alert(vigencia_fin);
    var cotizacion = $('#txtCotizacion').val();
    //alert(servicio_domicilio);
    var calle = $('#txtCalle').val();
    //alert(servicio_domicilio);
    var colonia = $('#txtColonia').val();
    //alert(servicio_domicilio);
    var numero = $('#txtNumero').val();
    //alert(servicio_domicilio);
    var codigo_postal = $('#txtCodigo_postal').val();
    //alert(servicio_domicilio);
    var info = {
        idAnunciante: idAnunciante,
        nombre_comercial: nombre_comercial,
        logo: logo,
        mas_imagenes: mas_imagenes,
        descripcion: descripcion,
        cotizacion: cotizacion,
        calle: calle,
        colonia: colonia,
        numero: numero,
        codigo_postal: codigo_postal
    };
    $.ajax({
        type: "POST",
        async: true,
        url: "api/Anunciante/update",
        data: info
    }
    ).done(function (data) {
        if (data.result != null){
           
            //alert("¡Actualización exitosa del Empleado!");
            swal("Correcto!", "¡Actualización exitosa del Anunciante!", "success");
        }else{
            alert("¡No se puedo actualizar el Anunciante!");}
        mostrarAnunciantesAct();
    }
    );
}

function cargarModificarAnunciante(posicion) {
   
    anunciante = anunciantesAct[posicion]; 
    $.ajax({
        type: "GET",
        url: "anunciante.html",
        async: true
    }
    ).done(
            function (data)
            {                
                $('#txtIdAnunciante').val(anunciante.idAnunciante);

                $('#txtNombre_comercial').val(anunciante.nombre_comercial);
                $('#txtLogo').val(anunciante.logo);
                $('#txtMas_imagenes').val(anunciante.mas_imagenes);
                $('#txtDescripcion').val(anunciante.descripcion);
                $('#txtCotizacion').val(anunciante.cotizacion);
                $('#txtCalle').val(anunciante.calle);
                $('#txtColonia').val(anunciante.colonia);
                $('#txtNumero').val(anunciante.numero);
                $('#txtCodigo_postal').val(anunciante.codigo_postal);                
            }
    );
}

function buscarAnunciante() {
    var tableReg = document.getElementById('tablaAnunciantesAct');
    var gen = $('#searchAnunciante').val();
    var searchText = document.getElementById('searchAnunciante').value.toLowerCase();

    for (var i = 1; i < tableReg.rows.length; i++) {
        var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');

        var found = false;
        for (var j = 0; j < ((cellsOfRow.length) - 3) && !found; j++) {
            var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }
        if (foundp) {
            tableRegp.rows[ip].style.display = '';
        } else {
            tableRegp.rows[ip].style.display = 'none';
        }
}

function decidirAnunciante(){
    if (anunciante== null){
        guardarAnunciante();
    }else{
        guardarModificarAnunciante();      
    }
}