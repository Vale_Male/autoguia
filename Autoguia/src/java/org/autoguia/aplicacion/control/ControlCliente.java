/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.control;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import org.autoguia.aplicacion.modelo.Cliente;
import org.autoguia.aplicacion.modelo.Persona;
import org.autoguia.aplicacion.modelo.Usuario;

/**
 *
 * @author jose-
 */
public class ControlCliente {
    
    public boolean insert(Cliente c) throws Exception {
        String sql = "call  agregar_cliente(?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?, ?,?,?)";
        boolean respuesta = false;
        CallableStatement cstmt = null;
        ConexionMySQL objConn = new ConexionMySQL();
        Connection conn = null;
        System.out.println(sql);
        try {
            int idUsuario= cstmt.getInt(13);
            if(idUsuario !=0){
            conn = objConn.abrir();

            cstmt = conn.prepareCall(sql);
            cstmt.setString(1, c.getPersona().getNombre());
            cstmt.setString(2, c.getPersona().getApellidoPaterno());
            cstmt.setString(3, c.getPersona().getApellidoMaterno());
            cstmt.setString(4, c.getPersona().getCalle());
            cstmt.setString(5, c.getPersona().getColonia());
            cstmt.setString(6, c.getPersona().getNumero());
            cstmt.setInt(7, c.getPersona().getCodigo_postal());
            cstmt.setString(8, c.getPersona().getFoto());
            cstmt.setString(9, c.getPersona().getFechaNacimiento());
            cstmt.setString(10, c.getPersona().getFechaInicio());
            cstmt.setInt(11, c.getPersona().getGenero());
            cstmt.setString(12, c.getPersona().getTelefono());
            
            cstmt.setString(13, c.getUsuario().getUsuario());
            cstmt.setString(14, c.getUsuario().getContrasenia());
            
            cstmt.setString(15, c.getCorreo());
            
            cstmt.registerOutParameter(16, Types.INTEGER);
            cstmt.registerOutParameter(17, Types.INTEGER);
            cstmt.registerOutParameter(18, Types.INTEGER);
            cstmt.executeUpdate();
            c.setIdCliente(cstmt.getInt(16));
            c.getPersona().setIdPersona(cstmt.getInt(17));
            c.getUsuario().setIdUsuario(cstmt.getInt(18));
            
            respuesta = true;
            cstmt.close();
            objConn.cerrar();
                System.out.println("Exito");
            } else{
                System.out.println("error");
                cstmt.close();
            objConn.cerrar();
            }
        } catch (Exception ex) {
            if (cstmt != null) {
                System.out.println("Error de insertar Cliente: " + ex.toString());
                cstmt.close();
            }
            objConn.cerrar();

            throw ex;
        }
        return respuesta;
    }
    
    public String insertC(Cliente c) throws Exception {
        ConexionMySQL objConn = new ConexionMySQL();
        Connection conn = null;
        //System.out.println(sql);
        try {
            
            conn = objConn.abrir();
            String sql = "call  agregarUsuario(?, ?, ?);";
            
            CallableStatement stmt = conn.prepareCall(sql);
            
            stmt.setString(1, c.getUsuario().getUsuario());
            stmt.setString(2, c.getUsuario().getContrasenia());
            stmt.registerOutParameter(3, Types.INTEGER);
            stmt.execute();
            int idUsuario = stmt.getInt(3);
            String me="";
            if (idUsuario !=0){
                me ="OK";
                
                String sql2 ="call agregarCliente(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                CallableStatement stmt2 = conn.prepareCall(sql2);
                stmt2.setString(1, c.getPersona().getNombre());
            stmt2.setString(2, c.getPersona().getApellidoPaterno());
            stmt2.setString(3, c.getPersona().getApellidoMaterno());
            stmt2.setString(4, c.getPersona().getCalle());
            stmt2.setString(5, c.getPersona().getColonia());
            stmt2.setString(6, c.getPersona().getNumero());
            stmt2.setInt(7, c.getPersona().getCodigo_postal());
            stmt2.setString(8, c.getPersona().getFoto());
            stmt2.setString(9, c.getPersona().getFechaNacimiento());
            stmt2.setString(10, c.getPersona().getFechaInicio());
            stmt2.setInt(11, c.getPersona().getGenero());
            stmt2.setString(12, c.getPersona().getTelefono());
            stmt2.setString(13, c.getCorreo());
            
            stmt2.registerOutParameter(14, Types.INTEGER);
            c.setIdCliente(stmt2.getInt(14));
            stmt2.setInt(15, idUsuario);
            stmt2.registerOutParameter(16, Types.INTEGER);
            c.getPersona().setIdPersona(stmt2.getInt(16));
            stmt2.execute();
            stmt.close();
            stmt2.close();
            objConn.cerrar();
            return me;
            } else{
                stmt.close();
                me="error";
                return me;
            }
            
        } catch (Exception ex) {
                System.out.println("Error de insertar cliente: " + ex.toString());
            objConn.cerrar();
            throw ex;
        }
    }
    
    public void insertU(Cliente c) throws Exception {
        String sql = "call  agregar_cliente(?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?, ?,?,?)";

        CallableStatement cstmt = null;
        ConexionMySQL objConn = new ConexionMySQL();
        Connection conn = null;
        System.out.println(sql);
        try {
            conn = objConn.abrir();

            cstmt = conn.prepareCall(sql);
            cstmt.setString(1, c.getPersona().getNombre());
            cstmt.setString(2, c.getPersona().getApellidoPaterno());
            cstmt.setString(3, c.getPersona().getApellidoMaterno());
            cstmt.setString(4, c.getPersona().getCalle());
            cstmt.setString(5, c.getPersona().getColonia());
            cstmt.setString(6, c.getPersona().getNumero());
            cstmt.setInt(7, c.getPersona().getCodigo_postal());
            cstmt.setString(8, c.getPersona().getFoto());
            cstmt.setString(9, c.getPersona().getFechaNacimiento());
            cstmt.setString(10, c.getPersona().getFechaInicio());
            cstmt.setInt(11, c.getPersona().getGenero());
            cstmt.setString(12, c.getPersona().getTelefono());
            
            cstmt.setString(13, c.getUsuario().getUsuario());
            cstmt.setString(14, c.getUsuario().getContrasenia());
            
            cstmt.setString(15, c.getCorreo());
            
            cstmt.registerOutParameter(16, Types.INTEGER);
            cstmt.registerOutParameter(17, Types.INTEGER);
            cstmt.registerOutParameter(18, Types.INTEGER);
            cstmt.executeUpdate();
            c.setIdCliente(cstmt.getInt(16));
            c.getPersona().setIdPersona(cstmt.getInt(17));
            c.getUsuario().setIdUsuario(cstmt.getInt(18));

            cstmt.close();
            objConn.cerrar();
        } catch (Exception ex) {
            if (cstmt != null) {
                System.out.println("Error de insertar Cliente: " + ex.toString());
                cstmt.close();
            }
            objConn.cerrar();

            throw ex;
        }
    }
    
    public ArrayList<Cliente> selectAll() throws Exception {
        String sql = "select * from v_cliente where estatus=1;";
        ConexionMySQL c = new ConexionMySQL();
        ArrayList<Cliente> cliente = new ArrayList<>();
        try {
            c.abrir();
            Connection conn = c.getConexion();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Persona objP = new Persona();
                objP.setIdPersona(rs.getInt(1));
                objP.setNombre(rs.getString(2));
                objP.setApellidoPaterno(rs.getString(3));
                objP.setApellidoMaterno(rs.getString(4));
                objP.setCalle(rs.getString(5));
                objP.setColonia(rs.getString(6));
                objP.setNumero(rs.getString(7));
                objP.setCodigo_postal(rs.getInt(8));
                objP.setFoto(rs.getString(9));
                objP.setFechaNacimiento(rs.getString(10));
                objP.setFechaInicio(rs.getString(11));
                objP.setGenero(rs.getInt(12));
                objP.setTelefono(rs.getString(13));
                
                Usuario objU = new Usuario();
                objU.setIdUsuario(rs.getInt(14));
                objU.setUsuario(rs.getString(15));
                objU.setContrasenia(rs.getString(16));
                
                Cliente objC= new Cliente();
                objC.setIdCliente(rs.getInt(17));
                objC.setCorreo(rs.getString(18));
                objC.setEstatus(rs.getInt(19));
                objC.setPersona(objP);
                objC.setUsuario(objU);
                cliente.add(objC);
            }
            rs.close();
            stmt.close();
            c.cerrar();
        }catch (Exception e) {
            System.out.println(e.toString());
        }
        return cliente;
    }
    
    public boolean registrarUsuario(Cliente c) throws Exception {
        String query = "call  agregar_cliente(?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?, ?,?,?);";
        ConexionMySQL con = new ConexionMySQL();
        boolean respuesta = false;
        try {
            con.abrir();
            Connection co = con.getConexion();
            CallableStatement cstmt = co.prepareCall(query);
            
            cstmt.setString(1, c.getPersona().getNombre());
            cstmt.setString(2, c.getPersona().getApellidoPaterno());
            cstmt.setString(3, c.getPersona().getApellidoMaterno());
            cstmt.setString(4, c.getPersona().getCalle());
            cstmt.setString(5, c.getPersona().getColonia());
            cstmt.setString(6, c.getPersona().getNumero());
            cstmt.setInt(7, c.getPersona().getCodigo_postal());
            cstmt.setString(8, c.getPersona().getFoto());
            cstmt.setString(9, c.getPersona().getFechaNacimiento());
            cstmt.setString(10, c.getPersona().getFechaInicio());
            cstmt.setInt(11, c.getPersona().getGenero());
            cstmt.setString(12, c.getPersona().getTelefono());
            
            cstmt.setString(13, c.getUsuario().getUsuario());
            cstmt.setString(14, c.getUsuario().getContrasenia());
            
            cstmt.setString(15, c.getCorreo());
            
            cstmt.registerOutParameter(16, Types.INTEGER);
            cstmt.registerOutParameter(17, Types.INTEGER);
            cstmt.registerOutParameter(18, Types.INTEGER);
            cstmt.executeUpdate();
            c.setIdCliente(cstmt.getInt(16));
            c.getPersona().setIdPersona(cstmt.getInt(17));
            c.getUsuario().setIdUsuario(cstmt.getInt(18));
            
            respuesta = true;
            System.out.println("el usuario fue: "+respuesta);
            cstmt.execute();
            cstmt.close();
            co.close();
            con.cerrar();
        } catch (SQLException ex) {
            do {
                System.out.println(ex.getErrorCode());
                System.out.println(ex);
                ex.printStackTrace();
                ex = ex.getNextException();
            } while (ex != null);
        }
        return respuesta;

    }
}
