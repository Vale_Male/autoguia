/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.control;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.autoguia.aplicacion.modelo.Anunciante;
import org.autoguia.aplicacion.modelo.Imagenes;
import org.autoguia.aplicacion.modelo.Persona;
import org.autoguia.aplicacion.modelo.UsuarioLogin;
import org.autoguia.aplicacion.modelo.Vendedor;
import org.autoguia.aplicacion.modelo.Zona;

/**
 *
 * @author jose-
 */
public class ControlAnunciante {
    public boolean insertAnunciante(Anunciante anunciante){
        ConexionMySQL conexion = new ConexionMySQL();
        boolean resp= false; 
        try {
            Connection c = conexion.abrir();
            String sql = "call insertarAnunciante(?,?,?,?,?,?,?,?,?,?,?,?);";
            try (CallableStatement stmt = (CallableStatement) c.prepareCall(sql)) {
                stmt.setString(1, anunciante.getNombre_comercial());
                stmt.setString(2, anunciante.getLogo());
                stmt.setInt(3, anunciante.getMas_imagenes());
                stmt.setString(4, anunciante.getDescripcion());
                stmt.setInt(5, anunciante.getCotizacion());
                stmt.setString(6, anunciante.getCalle());
                stmt.setString(7, anunciante.getColonia());
                stmt.setString(8, anunciante.getNumero());
                stmt.setInt(9, anunciante.getCodigo_postal());
                
                stmt.setString(10, anunciante.getImagenes().getImagen());
                
                stmt.setInt(11, anunciante.getVendedor().getIdVendedor());
                
                stmt.setInt(12, anunciante.getFiltroAnunciante().getFiltros().getIdFiltro());

                resp=true;
                stmt.execute();
            }
            conexion.cerrar();
        } catch (Exception e) {
            e.printStackTrace();
            resp=false;
        }
        return resp;     
    }
    
    public ArrayList<Anunciante> selectAll() throws Exception {
        String sql = "select * from vistaAnunciante;";
        ConexionMySQL c = new ConexionMySQL();
        ArrayList<Anunciante> anunciante = new ArrayList<>();
        try {
            c.abrir();
            Connection conn = c.getConexion();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Persona objP =new Persona();
                objP.setIdPersona(rs.getInt(1));
                objP.setNombre(rs.getString(2));
                objP.setApellidoPaterno(rs.getString(3));
                objP.setApellidoMaterno(rs.getString(4));
                objP.setCalle(rs.getString(5));
                objP.setColonia(rs.getString(6));
                objP.setNumero(rs.getString(7));
                objP.setCodigo_postal(rs.getInt(8));
                objP.setFoto(rs.getString(9));
                objP.setFechaNacimiento(rs.getString(10));
                objP.setFechaInicio(rs.getString(11));
                objP.setGenero(rs.getInt(12));
                objP.setTelefono(rs.getString(13));
                UsuarioLogin objU =new UsuarioLogin();
                objU.setUsuario(rs.getString(14));
                objU.setContrasenia(rs.getString(15));
                objU.setRol(rs.getString(16));
                objU.setEstatus(rs.getInt(17));
                Zona objZ=new Zona();
                objZ.setIdZonas(rs.getInt(18));
                objZ.setNombre_zona(rs.getString(19));
                Vendedor objV=new Vendedor();
                objV.setIdVendedor(rs.getInt(20));
                objV.setPersona(objP);
                objV.setUsuario(objU);
                objV.setZonas(objZ);
                Anunciante objA = new Anunciante();
                objA.setVendedor(objV);
                objA.setNombre_comercial(rs.getString(21));
                objA.setLogo(rs.getString(22));
                objA.setMas_imagenes(rs.getInt(23));
                objA.setDescripcion(rs.getString(24));
                objA.setCotizacion(rs.getInt(25));
                objA.setCalle(rs.getString(26));
                objA.setColonia(rs.getString(27));
                objA.setNumero(rs.getString(28));
                objA.setCodigo_postal(rs.getInt(29));
                objA.setEstatus(rs.getInt(30));
                Imagenes ima = new Imagenes();
                ima.setImagen(rs.getString(31));
                objA.setImagenes(ima);
                anunciante.add(objA);
            }
            rs.close();
            stmt.close();
            c.cerrar();
        }catch (Exception e) {
            System.out.println(e.toString());
        }
        return anunciante;
    }
    
    public boolean deleteAnunciante(int idAnunciante){
        ConexionMySQL conexion = new ConexionMySQL();
        boolean resp= true; 
        try {
            Connection c = conexion.abrir();
            String sql = " call eliminarAnunciante(?)";
            java.sql.CallableStatement stmt = (java.sql.CallableStatement) c.prepareCall(sql);
            stmt.setInt(1, idAnunciante);
            stmt.execute();
            stmt.close();
            conexion.cerrar();
        } catch (Exception e) {
            e.printStackTrace(); 
            resp=false;

        }
        return  resp;
    }
    
    public boolean updateAnunciante (Anunciante anunciante) {
        ConexionMySQL conexion = new ConexionMySQL();
         boolean resp= false; 
        try {
            Connection c = conexion.abrir();
            String sql = " call updateAnunciantes(?,?,?,?,?,?,?,?,?,?);";
            try (CallableStatement stmt = (CallableStatement) c.prepareCall(sql)) {
                stmt.setString(1, anunciante.getNombre_comercial());
                stmt.setString(2, anunciante.getLogo());
                stmt.setInt(3, anunciante.getMas_imagenes());
                stmt.setString(4, anunciante.getDescripcion());
                stmt.setInt(5, anunciante.getCotizacion());
                stmt.setString(6, anunciante.getCalle());
                stmt.setString(7, anunciante.getColonia());
                stmt.setString(8, anunciante.getNumero());
                stmt.setInt(9, anunciante.getCodigo_postal());
                stmt.setInt(10, anunciante.getIdAnunciante());
                resp=true;
                stmt.execute();
            }
            conexion.cerrar();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return resp;
    }
}
