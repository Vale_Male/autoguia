/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.control;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import org.autoguia.aplicacion.modelo.Anunciante;
import org.autoguia.aplicacion.modelo.Imagenes;
import org.autoguia.aplicacion.modelo.Promociones;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 21/09/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 22/09/2020 Controlador
 */
public class ControlPromocion {
    
    public ArrayList<Promociones> selectAll() throws Exception {
        String sql = "select * from v_promocion;";
        ConexionMySQL c = new ConexionMySQL();
        ArrayList<Promociones> promociones = new ArrayList<>();
        try {
            c.abrir();
            Connection conn = c.getConexion();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promociones objP = new Promociones();
                objP.setIdPromocion(rs.getInt(1));
                objP.setImagen_base(rs.getString(2));
                objP.setDescripcion(rs.getString(3));
                objP.setVigencia_inicio(rs.getString(4));
                objP.setVigencia_fin(rs.getString(5));
                objP.setServicio_domicilio(rs.getInt(6));
                Anunciante objA = new Anunciante();
                objA.setIdAnunciante(rs.getInt(7));
                objA.setNombre_comercial(rs.getString(8));
                objA.setLogo(rs.getString(9));
                //objA.setImagen_promocion(rs.getString(10));
                objA.setMas_imagenes(rs.getInt(10));
                objA.setDescripcion(rs.getString(11));
                objA.setCotizacion(rs.getInt(12));
                objA.setCalle(rs.getString(13));
                objA.setColonia(rs.getString(14));
                objA.setNumero(rs.getString(15));
                objA.setCodigo_postal(rs.getInt(16));
                objP.setAnunciante(objA);
                promociones.add(objP);
            }
            rs.close();
            stmt.close();
            c.cerrar();
        }catch (Exception e) {
            System.out.println(e.toString());
        }
        return promociones;
    }
    
    public void insert(Promociones p) throws Exception {
        String sql = "call  agregar_promocion(?, ?, ?, ?, ?, ?, ?)";
        CallableStatement cstmt = null;
        ConexionMySQL objConn = new ConexionMySQL();
        Connection conn = null;
        System.out.println(sql);
        try {
            conn = objConn.abrir();
            cstmt = conn.prepareCall(sql);
            cstmt.setString(1, p.getImagen_base());
            cstmt.setString(2, p.getDescripcion());
            cstmt.setString(3, p.getVigencia_inicio());
            cstmt.setString(4, p.getVigencia_fin());
            cstmt.setInt(5, p.getServicio_domicilio());
            cstmt.setInt(6, p.getAnunciante().getIdAnunciante());

            cstmt.registerOutParameter(7, Types.INTEGER);
            cstmt.executeUpdate();
            p.setIdPromocion(cstmt.getInt(7));
            cstmt.close();
            objConn.cerrar();
        } catch (Exception ex) {
            if (cstmt != null) {
                System.out.println("Error de insertar Promociones: " + ex.toString());
                cstmt.close();
            }
            objConn.cerrar();
            throw ex;
        }
    }
    
    public void update(Promociones p) throws Exception {
        String sql = "call actualizarPromocion(?, ?, ?, ?, ?,?)";
        CallableStatement cstmt = null;
        ConexionMySQL objConn = new ConexionMySQL();
        Connection conn = null;
        System.out.println(sql);
        try {
            conn = objConn.abrir();
            conn.setAutoCommit(false);
            cstmt = conn.prepareCall(sql);

            cstmt.setInt(1, p.getIdPromocion());
            cstmt.setString(2, p.getImagen_base());
            cstmt.setString(3, p.getDescripcion());
            cstmt.setString(4, p.getVigencia_inicio());
            cstmt.setString(5, p.getVigencia_fin());
            cstmt.setInt(6, p.getServicio_domicilio());

            cstmt.executeUpdate();
            conn.commit();
            cstmt.close();
            objConn.cerrar();
        } catch (Exception ex) {
            if (cstmt != null) {
                conn.rollback();
                cstmt.close();
            }
            objConn.cerrar();
            throw ex;
        }
    }
    
    public void delete(int idP) throws Exception {
        String sql = "call eliminarPromocion(?)";

        CallableStatement cstmt = null;
        ConexionMySQL objConn = new ConexionMySQL();
        Connection conn = null;
        try {
            conn = objConn.abrir();
            cstmt = conn.prepareCall(sql);
            cstmt.setInt(1, idP);
            cstmt.executeUpdate();

            cstmt.close();
            objConn.cerrar();
        } catch (Exception ex) {
            if (cstmt != null) {
                cstmt.close();
            }

            objConn.cerrar();
            throw ex;
        }
    }
}
