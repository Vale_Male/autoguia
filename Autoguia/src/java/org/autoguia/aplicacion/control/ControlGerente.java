/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.control;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.autoguia.aplicacion.modelo.Ciudad;
import org.autoguia.aplicacion.modelo.Gerente;
import org.autoguia.aplicacion.modelo.Persona;
import org.autoguia.aplicacion.modelo.UsuarioLogin;

/**
 *
 * @author guita
 */
public class ControlGerente {
    public boolean insertGerente(Gerente gerente){
        ConexionMySQL conexion = new ConexionMySQL();
        boolean resp= false; 
        try {
            Connection c = conexion.abrir();
            String sql = "call insertarGerente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
            try (CallableStatement stmt = (CallableStatement) c.prepareCall(sql)) {
                stmt.setString(1, gerente.getPersona().getNombre());
                stmt.setString(2, gerente.getPersona().getApellidoPaterno());
                stmt.setString(3, gerente.getPersona().getApellidoMaterno());
                stmt.setString(4, gerente.getPersona().getCalle());
                stmt.setString(5, gerente.getPersona().getColonia());
                stmt.setString(6, gerente.getPersona().getNumero());
                stmt.setInt(7, gerente.getPersona().getCodigo_postal());
                stmt.setString(8, gerente.getPersona().getFoto());
                stmt.setString(9, gerente.getPersona().getFechaNacimiento());
                stmt.setString(10, gerente.getPersona().getFechaInicio());
                stmt.setInt(11, gerente.getPersona().getGenero());
                stmt.setString(12, gerente.getPersona().getTelefono());
                
                stmt.setString(13, gerente.getUsuario().getUsuario());
                stmt.setString(14, gerente.getUsuario().getContrasenia());
                stmt.setString(15, gerente.getUsuario().getToken());
                stmt.setString(16, gerente.getUsuario().getRol());
                
                stmt.setString(17, gerente.getCiudad().getNombre_ciudad());                
                stmt.setString(18, gerente.getCiudad().getEstado());

                resp=true;
                stmt.execute();
            }
            conexion.cerrar();
        } catch (Exception e) {
            e.printStackTrace();
            resp=false;
        }
        return resp;     
    }
    
    public ArrayList<Gerente> selecAll() throws SQLException, Exception{
        String sql = "SELECT * FROM AllGerentes WHERE estatus = 1 ORDER BY nombre";
        ConexionMySQL objConn = new ConexionMySQL();
        Connection conn = null;
        Statement stmt = null;
        ArrayList<Gerente> gerentes = new ArrayList<>();
        try {
            conn = objConn.abrir();
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
                while (rs.next()) {
                 Persona objP=new Persona();
                 objP.setIdPersona(rs.getInt(2));
                 objP.setNombre(rs.getString(5));
                 objP.setApellidoPaterno(rs.getString(6));
                 objP.setApellidoMaterno(rs.getString(7));
                 objP.setCalle(rs.getString(8));
                 objP.setColonia(rs.getString(9));
                 objP.setNumero(rs.getString(10));
                 objP.setCodigo_postal(rs.getInt(11));
                 objP.setFoto(rs.getString(12));
                 objP.setFechaNacimiento(rs.getString(13));
                 objP.setFechaInicio(rs.getString(14));
                 objP.setGenero(rs.getInt(15));
                 objP.setTelefono(rs.getString(16));
                 
                 UsuarioLogin objU = new UsuarioLogin(rs.getInt(3), rs.getString(17), rs.getString(18), rs.getString(19), 1, rs.getString(21));
                 
                 Ciudad objC = new Ciudad(rs.getInt(4), rs.getString(22), rs.getString(23));
                 
                 Gerente objG=new Gerente(rs.getInt(1), objP, objU, objC);
                 
                 gerentes.add(objG);      
                }
            
            rs.close();
            stmt.close();
            objConn.cerrar();
        } catch (Exception ex) {
            if (stmt != null) {
                System.out.println("Error de consultar gerente: " + ex.toString());
                stmt.close();
            }
            objConn.cerrar();
            throw ex;
        }
        
        return gerentes;
    }
    
    public ArrayList<Gerente> selecAllDelete() throws SQLException, Exception{
        String sql = "SELECT * FROM AllGerentes WHERE estatus = 0 ORDER BY nombre";
        ConexionMySQL objConn = new ConexionMySQL();
        Connection conn = null;
        Statement stmt = null;
        ArrayList<Gerente> gerentes = new ArrayList<>();
        try {
            conn = objConn.abrir();
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
                while (rs.next()) {
                 Persona objP=new Persona();
                 objP.setIdPersona(rs.getInt(2));
                 objP.setNombre(rs.getString(5));
                 objP.setApellidoPaterno(rs.getString(6));
                 objP.setApellidoMaterno(rs.getString(7));
                 objP.setCalle(rs.getString(8));
                 objP.setColonia(rs.getString(9));
                 objP.setNumero(rs.getString(10));
                 objP.setCodigo_postal(rs.getInt(11));
                 objP.setFoto(rs.getString(12));
                 objP.setFechaNacimiento(rs.getString(13));
                 objP.setFechaInicio(rs.getString(14));
                 objP.setGenero(rs.getInt(15));
                 objP.setTelefono(rs.getString(16));
                 
                 UsuarioLogin objU = new UsuarioLogin(rs.getInt(3), rs.getString(17), rs.getString(18), rs.getString(19), 1, rs.getString(21));
                 
                 Ciudad objC = new Ciudad(rs.getInt(4), rs.getString(22), rs.getString(23));
                 
                 Gerente objG=new Gerente(rs.getInt(1), objP, objU, objC);
                 
                 gerentes.add(objG);      
                }
            
            rs.close();
            stmt.close();
            objConn.cerrar();
        } catch (Exception ex) {
            if (stmt != null) {
                System.out.println("Error de consultar gerente: " + ex.toString());
                stmt.close();
            }
            objConn.cerrar();
            throw ex;
        }
        
        return gerentes;
    }
    
    public boolean deleteGerente(int idGerente){
        ConexionMySQL conexion = new ConexionMySQL();
        boolean resp= true; 
        try {
            Connection c = conexion.abrir();
            String sql = " call eliminarGerente(?)";
            java.sql.CallableStatement stmt = (java.sql.CallableStatement) c.prepareCall(sql);
            stmt.setInt(1, idGerente);
            stmt.execute();
            stmt.close();
            conexion.cerrar();
        } catch (Exception e) {
            e.printStackTrace();
            resp=false;

        }
        return  resp;
    }
    
    public boolean reactivarGerente(int idGerente){
        ConexionMySQL conexion = new ConexionMySQL();
        boolean resp= true; 
        try {
            Connection c = conexion.abrir();
            String sql = " call activarGerente(?)";
            java.sql.CallableStatement stmt = (java.sql.CallableStatement) c.prepareCall(sql);
            stmt.setInt(1, idGerente);
            stmt.execute();
            stmt.close();
            conexion.cerrar();
        } catch (Exception e) {
            e.printStackTrace();
            resp=false;

        }
        return  resp;
    }
    
    public boolean updateGerente (Gerente gerentes) {
        ConexionMySQL conexion = new ConexionMySQL();
         boolean resp= false; 
        try {
            Connection c = conexion.abrir();
            String sql = " call updateGerentes(?,?,?,?,?,?,?,?,?,?,?,?,?);";
            try (CallableStatement stmt = (CallableStatement) c.prepareCall(sql)) {
                stmt.setString(1, gerentes.getPersona().getNombre());
                stmt.setString(2, gerentes.getPersona().getApellidoPaterno());
                stmt.setString(3, gerentes.getPersona().getApellidoMaterno());
                stmt.setString(4, gerentes.getPersona().getCalle());
                stmt.setString(5, gerentes.getPersona().getColonia());
                stmt.setString(6, gerentes.getPersona().getNumero());
                stmt.setInt(7, gerentes.getPersona().getCodigo_postal());
                stmt.setString(8, gerentes.getPersona().getFoto());
                stmt.setString(9, gerentes.getPersona().getFechaNacimiento());
                stmt.setString(10, gerentes.getPersona().getFechaInicio());
                stmt.setInt(11, gerentes.getPersona().getGenero());
                stmt.setString(12, gerentes.getPersona().getTelefono());
                stmt.setInt(13, gerentes.getIdGerente());
                resp=true;
                stmt.execute();
            }
            conexion.cerrar();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return resp;
    }
}
