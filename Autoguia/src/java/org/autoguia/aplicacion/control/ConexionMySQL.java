package org.autoguia.aplicacion.control;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Valeria Magdalena Sanchez Gonzalez
 * @version 1.0
 * @Fecha 19/09/2020
 * @Email valerybtrmagda@gmail.com
 * @Comentarios 19/09/2020 conexion
 */

public class ConexionMySQL{

    String userName;
    String password;
    String url;
    Connection conautoguia;
    
    public ConexionMySQL(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            userName="root";
            password="root";
            url="jdbc:mysql://localhost:3306/Autoguia";
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }
    
    public Connection abrir() throws Exception{
        conautoguia = DriverManager.getConnection(url, userName, password);
        return conautoguia;
    }
    
    public void cerrar() throws Exception{
        try{
        if (conautoguia != null) {
            conautoguia.close();
            conautoguia=null;
        }
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }
    
    public Connection getConexion(){
        return conautoguia;
    }
}