/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.prueba;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.autoguia.aplicacion.control.ControlAnunciante;
import org.autoguia.aplicacion.control.ControlGerente;
import org.autoguia.aplicacion.control.ControlPromocion;
import org.autoguia.aplicacion.control.ControlVendedor;
import org.autoguia.aplicacion.modelo.Anunciante;
import org.autoguia.aplicacion.modelo.Ciudad;
import org.autoguia.aplicacion.modelo.FiltroAnunciante;
import org.autoguia.aplicacion.modelo.Gerente;
import org.autoguia.aplicacion.modelo.Imagenes;
import org.autoguia.aplicacion.modelo.Persona;
import org.autoguia.aplicacion.modelo.Promociones;
import org.autoguia.aplicacion.modelo.UsuarioLogin;
import org.autoguia.aplicacion.modelo.Vendedor;
import org.autoguia.aplicacion.modelo.Zona;

/**
 *
 * @author jose-
 */
public class MainClas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

//        ControlAnunciante ca = new ControlAnunciante();
//        Anunciante anunciante = new Anunciante();
//        anunciante.setNombre_comercial("BROWA");
//        anunciante.setLogo("fffffff");
//        anunciante.setMas_imagenes(2);
//        anunciante.setDescripcion("Autopartes");
//        anunciante.setCotizacion(2500);
//        anunciante.setCalle("Oxígeno");
//        anunciante.setColonia("Valle de Señora");
//        anunciante.setNumero("205A");
//        anunciante.setCodigo_postal(34627);
//        anunciante.setIdAnunciante(7);
//        ca.updateAnunciante(anunciante);
//        Vendedor v = getVendedor();
//
//        Imagenes i = new Imagenes();
//        FiltroAnunciante fa = new FiltroAnunciante();
//        Anunciante a = new Anunciante(0, v, "BMW", "djhvbabxsjdncsb", 2, "General", 1500, "San Francisco", "San Miguel", "307A", 37216, 1, i, fa);
//        ca.insertAnunciante(a);
//      mostrarAnunciante();
        ControlGerente cg = new ControlGerente();
        Gerente gerente = new Gerente();
        Persona persona = new Persona(1, "Cesar", "Soto", "Cervantes", "San Vicente de Paúl", 
                "Santa Rosa de Lima","301B", 37210, "csfhgsrfsgcfcsfdfs", "1999-09-18", "2020-08-23", 1,
                "4775265006");
        gerente.setPersona(persona);
        gerente.setIdGerente(1);

        cg.updateGerente(gerente);
       mostrarGerente();
    }

    public static void mostrarGerente() throws Exception {
        ControlGerente cg = new ControlGerente();
        ArrayList<Gerente> r = cg.selecAll();
        for (int i = 0; i < r.size(); i++) {
            
            System.out.println(r.get(i));
        }
    }


    public static void mostrarVendedor() throws Exception {
        ControlVendedor cv = new ControlVendedor();
        ArrayList<Vendedor> r = cv.selectAll();
        for (int i = 0; i < r.size(); i++) {
            System.out.println(r.get(i));
        }
    }

    public static void mostrarAnunciante() throws Exception {
        ControlAnunciante ca = new ControlAnunciante();
        ArrayList<Anunciante> r = ca.selectAll();
        for (int i = 0; i < r.size(); i++) {
            System.out.println(r.get(i));
        }
    }
    
    public static Vendedor getVendedor() throws Exception {
        ControlVendedor cv = new ControlVendedor();
        ArrayList<Vendedor> r = cv.selectAll();

          Vendedor ven =   r.get(0);
          
        return ven;
    }

}
