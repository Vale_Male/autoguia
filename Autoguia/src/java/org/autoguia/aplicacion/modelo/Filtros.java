/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.modelo;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 20/09/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 22/09/2020 modelo
 */
public class Filtros {

    private int idFiltro;
    private String fltro;

    public int getIdFiltro() {
        return idFiltro;
    }

    public void setIdFiltro(int idFiltro) {
        this.idFiltro = idFiltro;
    }

    public String getFltro() {
        return fltro;
    }

    public void setFltro(String fltro) {
        this.fltro = fltro;
    }

    public Filtros(int idFiltro, String fltro) {
        this.idFiltro = idFiltro;
        this.fltro = fltro;
    }
    
    
}
