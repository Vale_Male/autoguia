/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.modelo;

import java.util.Date;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 20/09/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 22/09/2020 modelo
 */
public class Horario {
    private int idHorario;
    private Anunciante anunciante;
    private String dia;
    private Date horaInicio;
    private Date horaFin;

    public int getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(int idHorario) {
        this.idHorario = idHorario;
    }

    public Anunciante getAnunciante() {
        return anunciante;
    }

    public void setAnunciante(Anunciante anunciante) {
        this.anunciante = anunciante;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public Date getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(Date horaFin) {
        this.horaFin = horaFin;
    }

    public Horario(int idHorario, Anunciante anunciante, String dia, Date horaInicio, Date horaFin) {
        this.idHorario = idHorario;
        this.anunciante = anunciante;
        this.dia = dia;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
    }
    
    
}
