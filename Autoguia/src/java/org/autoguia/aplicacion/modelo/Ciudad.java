/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.modelo;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 20/09/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 22/09/2020 modelo
 */
public class Ciudad {
    private int idCiudad;
    private String nombre_ciudad;
    private String estado;

    public int getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(int idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getNombre_ciudad() {
        return nombre_ciudad;
    }

    public void setNombre_ciudad(String nombre_ciudad) {
        this.nombre_ciudad = nombre_ciudad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Ciudad() {
    }

    public Ciudad(int idCiudad, String nombre_ciudad, String estado) {
        this.idCiudad = idCiudad;
        this.nombre_ciudad = nombre_ciudad;
        this.estado = estado;
    }
    
    public Ciudad( String nombre_ciudad, String estado) {
        this.nombre_ciudad = nombre_ciudad;
        this.estado = estado;
    }
}
