/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.modelo;

import java.util.Date;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 20/09/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 22/09/2020 modelo
 */
public class Promociones {
    
   private int idPromocion;
   private String imagen_base;
   private String descripcion;
   private String vigencia_inicio;
   private String vigencia_fin;
   private int servicio_domicilio;
   private Anunciante anunciante;
   private Imagenes imagen;

    public int getIdPromocion() {
        return idPromocion;
    }

    public void setIdPromocion(int idPromocion) {
        this.idPromocion = idPromocion;
    }

    public String getImagen_base() {
        return imagen_base;
    }

    public void setImagen_base(String imagen_base) {
        this.imagen_base = imagen_base;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getVigencia_inicio() {
        return vigencia_inicio;
    }

    public void setVigencia_inicio(String vigencia_inicio) {
        this.vigencia_inicio = vigencia_inicio;
    }

    public String getVigencia_fin() {
        return vigencia_fin;
    }

    public void setVigencia_fin(String vigencia_fin) {
        this.vigencia_fin = vigencia_fin;
    }

    public int getServicio_domicilio() {
        return servicio_domicilio;
    }

    public void setServicio_domicilio(int servicio_domicilio) {
        this.servicio_domicilio = servicio_domicilio;
    }

    public Anunciante getAnunciante() {
        return anunciante;
    }

    public void setAnunciante(Anunciante anunciante) {
        this.anunciante = anunciante;
    }

    public Imagenes getImagen() {
        return imagen;
    }

    public void setImagen(Imagenes imagen) {
        this.imagen = imagen;
    }

    public Promociones(int idPromocion, String imagen_base, String descripcion, String vigencia_inicio, String vigencia_fin, int servicio_domicilio, Anunciante anunciante, Imagenes imagen) {
        this.idPromocion = idPromocion;
        this.imagen_base = imagen_base;
        this.descripcion = descripcion;
        this.vigencia_inicio = vigencia_inicio;
        this.vigencia_fin = vigencia_fin;
        this.servicio_domicilio = servicio_domicilio;
        this.anunciante = anunciante;
        this.imagen = imagen;
    }
    
    public Promociones(String imagen_base, String descripcion, String vigencia_inicio, String vigencia_fin, int servicio_domicilio, Anunciante anunciante) {
        this.imagen_base = imagen_base;
        this.descripcion = descripcion;
        this.vigencia_inicio = vigencia_inicio;
        this.vigencia_fin = vigencia_fin;
        this.servicio_domicilio = servicio_domicilio;
        this.anunciante = anunciante;
    }
    
    public Promociones(int idPromocion, String imagen_base, String descripcion, String vigencia_inicio, String vigencia_fin, int servicio_domicilio) {
        this.idPromocion = idPromocion;
        this.imagen_base = imagen_base;
        this.descripcion = descripcion;
        this.vigencia_inicio = vigencia_inicio;
        this.vigencia_fin = vigencia_fin;
        this.servicio_domicilio = servicio_domicilio;
    }
    
    public Promociones() {
    }
      
}
