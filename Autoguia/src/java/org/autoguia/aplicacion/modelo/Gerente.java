/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.modelo;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 20/09/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 22/09/2020 modelo
 */
public class Gerente {
    
    private int idGerente;
    private UsuarioLogin usuario;
    private Ciudad ciudad;
    private Persona persona;

    public int getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(int idGerente) {
        this.idGerente = idGerente;
    }

    public UsuarioLogin getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioLogin usuario) {
        this.usuario = usuario;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Gerente() {
    }

    public Gerente(int idGerente, Persona persona, UsuarioLogin usuario, Ciudad ciudad) {
        this.idGerente = idGerente;
        this.persona = persona;
        this.usuario = usuario;
        this.ciudad = ciudad;   
    }
    
    
}
