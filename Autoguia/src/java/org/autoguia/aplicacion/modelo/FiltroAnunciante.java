/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.modelo;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 20/09/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 22/09/2020 modelo
 */
public class FiltroAnunciante {

    private int idFil;
    private Filtros filtros;

    public int getIdFil() {
        return idFil;
    }

    public void setIdFil(int idFil) {
        this.idFil = idFil;
    }

    public Filtros getFiltros() {
        return filtros;
    }

    public void setFiltros(Filtros filtros) {
        this.filtros = filtros;
    }

    public FiltroAnunciante(int idFil, Filtros filtros) {
        this.idFil = idFil;
        this.filtros = filtros;
    }

    public FiltroAnunciante() {
    }

    @Override
    public String toString() {
        return "FiltroAnunciante{" + "idFil=" + idFil + ", filtros=" + filtros + '}';
    }

}
