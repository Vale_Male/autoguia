/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.rest;

import com.google.gson.Gson;
import java.util.ArrayList;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.autoguia.aplicacion.control.ControlCliente;
import org.autoguia.aplicacion.control.ControlUsuario;
import org.autoguia.aplicacion.control.ControlVendedor;
import org.autoguia.aplicacion.modelo.Cliente;
import org.autoguia.aplicacion.modelo.Persona;
import org.autoguia.aplicacion.modelo.Usuario;
import org.autoguia.aplicacion.modelo.Vendedor;

/**
 *
 * @author jose-
 */
@Path("cliente")
public class RESTCliente {
    @POST
    @Path("insert")
    @Produces(MediaType.APPLICATION_JSON)
    public Response insert(
            @FormParam("nombre") String nombre,
            @FormParam("apellidoP") String ap,
            @FormParam("apellidoM") String am,
            @FormParam("calle") String calle,
            @FormParam("colonia") String colonia,
            @FormParam("numero") String numero,
            @FormParam("codigop") int codigop,
            @FormParam("foto") String foto,
            @FormParam("fechaNacimiento") String fechaN,
            @FormParam("fechaInicio") String fechaI,
            @FormParam("genero") int genero,
            @FormParam("telefono") String telefono,
            @FormParam("usuario") String usuario,
            @FormParam("contrasenia") String contrasenia,
            @FormParam("correo") String correo
    ) {
        Persona objP= new Persona(nombre, ap, am, calle, colonia, numero, codigop, foto, fechaN, fechaI, genero, telefono);
        Usuario objU=new Usuario(usuario, contrasenia);
        Cliente objC= new Cliente(correo, objP, objU);
        ControlCliente objCC = new ControlCliente();
        String out = null;
        try {
            objCC.insertU(objC);
            out = "{\"result\":\"OK\"}";
        } catch (Exception err) {
            out = "{\"error\":\"ERROR\"}";
            err.printStackTrace();
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @POST
    @Path("insertC")
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertC(
            @FormParam("nombre") String nombre,
            @FormParam("apellidoP") String ap,
            @FormParam("apellidoM") String am,
            @FormParam("calle") String calle,
            @FormParam("colonia") String colonia,
            @FormParam("numero") String numero,
            @FormParam("codigop") int codigop,
            @FormParam("foto") String foto,
            @FormParam("fechaNacimiento") String fechaN,
            @FormParam("fechaInicio") String fechaI,
            @FormParam("genero") int genero,
            @FormParam("telefono") String telefono,
            @FormParam("usuario") String usuario,
            @FormParam("contrasenia") String contrasenia,
            @FormParam("correo") String correo
    ) {
        Persona objP= new Persona(nombre, ap, am, calle, colonia, numero, codigop, foto, fechaN, fechaI, genero, telefono);
        Usuario objU=new Usuario(usuario, contrasenia);
        Cliente objC= new Cliente(correo, objP, objU);
        ControlCliente objCC = new ControlCliente();
        String out = null;
        String v=null;
        try {
            v=objCC.insertC(objC);
            if (v !="error"){
                out = "{\"result\":\"OK\"}";
            }else{
                out = "{\"error\":\"error\"}";
            }
            
        } catch (Exception err) {
            err.printStackTrace();
            out = "{\"error\":\"ERROR\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @GET
    @Path("getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        ArrayList<Cliente> cliente = null;
        Gson json = new Gson();

        ControlCliente ctrlC = new ControlCliente();

        String salida = null;
        try {
            cliente = ctrlC.selectAll();
            salida = json.toJson(cliente);
        } catch (Exception e) {
            e.printStackTrace();
            salida = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(salida).build();
    }
}
