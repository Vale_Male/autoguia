/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.rest;

import com.google.gson.Gson;
import java.util.ArrayList;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.autoguia.aplicacion.control.ControlPromocion;
import org.autoguia.aplicacion.control.ControlVendedor;
import org.autoguia.aplicacion.modelo.Ciudad;
import org.autoguia.aplicacion.modelo.Persona;
import org.autoguia.aplicacion.modelo.Promociones;
import org.autoguia.aplicacion.modelo.UsuarioLogin;
import org.autoguia.aplicacion.modelo.Vendedor;
import org.autoguia.aplicacion.modelo.Zona;

/**
 *
 * @author Jose Eduardo Servin Sotelo
 * @version 1.0
 * @Fecha 05/10/2020
 * @Email jservinsotelo14@gmail.com
 * @Comentarios 05/10/2020 Rest
 */
@Path("vendedor")
public class RESTVendedor {
    
    @GET
    @Path("getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        ArrayList<Vendedor> vendedor = null;
        Gson json = new Gson();

        ControlVendedor ctrlV = new ControlVendedor();

        String salida = null;
        try {
            vendedor = ctrlV.selectAll();
            salida = json.toJson(vendedor);
        } catch (Exception e) {
            e.printStackTrace();
            salida = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(salida).build();
    }
    
    @GET
    @Path("getAllDelete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllDelete() {
        ArrayList<Vendedor> vendedor = null;
        Gson json = new Gson();

        ControlVendedor ctrlV = new ControlVendedor();

        String salida = null;
        try {
            vendedor = ctrlV.selectAllDelete();
            salida = json.toJson(vendedor);
        } catch (Exception e) {
            e.printStackTrace();
            salida = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(salida).build();
    }
    
    @POST
    @Path("insert")
    @Produces(MediaType.APPLICATION_JSON)
    public Response insert(
            @FormParam("nombre") String nombre,
            @FormParam("apellidoPaterno") String apellidoPaterno,
            @FormParam("apellidoMaterno") String apellidoMaterno,
            @FormParam("calle") String calle,
            @FormParam("colonia") String colonia,
            @FormParam("numero") String numero,
            @FormParam("codigo_postal") int codigo_postal,
            @FormParam("foto") String foto,
            @FormParam("fechaNacimiento") String fechaN,
            @FormParam("fechaInicio") String fechaI,
            @FormParam("genero") int genero,
            @FormParam("telefono") String telefono,
            @FormParam("usuario") String usuario,
            @FormParam("contrasenia") String contrasenia,
            @FormParam("status") int status,
            @FormParam("rol") String rol,
            @FormParam("nombre_ciudad") String nombre_ciudad,
            @FormParam("estado") String estado,
            @FormParam("zona") String zona
    ) {
        Persona objP= new Persona(nombre, apellidoPaterno, apellidoMaterno, calle, colonia, numero, codigo_postal, foto, fechaN, fechaI, genero, telefono);
        UsuarioLogin objU=new UsuarioLogin(usuario, contrasenia, "", status, rol);
        Ciudad objC=new Ciudad(nombre_ciudad, estado);
        Zona objZ= new Zona(objC, zona);
        Vendedor objV=new Vendedor(objP, objU, objZ);

        ControlVendedor objCV = new ControlVendedor();
        String out = null;

        try {
            objCV.insert(objV);
            out = "{\"result\":\"OK\"}";
        } catch (Exception err) {
            out = "{\"error\":\"ERROR\"}";
            err.printStackTrace();
        }

        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(
            @FormParam("idPersona") @DefaultValue("0") int idPersona,
            @FormParam("nombre") String nombre,
            @FormParam("apellidoPaterno") String apellidoPaterno,
            @FormParam("apellidoMaterno") String apellidoMaterno,
            @FormParam("calle") String calle,
            @FormParam("colonia") String colonia,
            @FormParam("numero") String numero,
            @FormParam("codigo_postal") int codigo_postal,
            @FormParam("foto") String foto,
            @FormParam("fechaNacimiento") String fechaNacimiento,
            @FormParam("genero") int genero,
            @FormParam("telefono") String telefono
    ) {

        Persona objP = new Persona(idPersona, nombre, apellidoPaterno, apellidoMaterno, calle, colonia, numero, codigo_postal, foto, fechaNacimiento, genero, telefono);
        Vendedor objV=new Vendedor();
        objV.setPersona(objP);
        ControlVendedor ctrlV = new ControlVendedor();
        String salida = null;
        try {
            ctrlV.update(objV);
            salida = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            salida = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(salida).build();
    }
    
    @POST
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@FormParam("idVendedor") @DefaultValue("1") int idVendedor) {

        ControlVendedor ctrlV = new ControlVendedor();

        String salida = null;
        try {
            ctrlV.delete(idVendedor);
            salida = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            salida = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(salida).build();
    }
    
    @POST
    @Path("active")
    @Produces(MediaType.APPLICATION_JSON)
    public Response active(@FormParam("idVendedor") @DefaultValue("1") int idVendedor) {

        ControlVendedor ctrlV = new ControlVendedor();

        String salida = null;
        try {
            ctrlV.active(idVendedor);
            salida = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            salida = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(salida).build();
    }
}
