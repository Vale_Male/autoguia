/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autoguia.aplicacion.rest;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.autoguia.aplicacion.control.ControlGerente;
import org.autoguia.aplicacion.modelo.Ciudad;
import org.autoguia.aplicacion.modelo.Gerente;
import org.autoguia.aplicacion.modelo.Persona;
import org.autoguia.aplicacion.modelo.UsuarioLogin;
/**
 *
 * @author guita
 */
@Path("Gerente")
public class RESTGerente extends Application{
    @GET
    @Path("getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        ControlGerente cg = new ControlGerente();
        ArrayList<Gerente> gerentes = null;
        Gson gson = new Gson();

        String out = null;
        try {
            gerentes = cg.selecAll();
            out = gson.toJson(gerentes);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @GET
    @Path("getAllDelete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllDelete() {
        ControlGerente cg = new ControlGerente();
        List<Gerente> gerentes = null;
        Gson gson = new Gson();

        String out = null;
        try {
            gerentes = cg.selecAllDelete();
            out = gson.toJson(gerentes);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @POST
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(
            @FormParam("idGerente") @DefaultValue("1") int idGerente) throws Exception{
            
        String out = "";
        ControlGerente cg = new ControlGerente();
        
        if(cg.deleteGerente(idGerente)){
                out = "{\"result\":\"OK\"}";
            }
            else {
                out = "{\"error\":\"ERROR\"}";
            }
        
       
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @POST
    @Path("reactivate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response reactivate(
            @FormParam("idGerente") @DefaultValue("1") int idGerente) throws Exception{
            
        String out = "";
        ControlGerente cg = new ControlGerente();
        
        if(cg.reactivarGerente(idGerente)){
                out = "{\"result\":\"OK\"}";
            }
            else {
                out = "{\"error\":\"ERROR\"}";
            }
        
       
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @POST
    @Path("insert")
    @Produces(MediaType.APPLICATION_JSON)
    public Response insert(   
        @FormParam("nombre") @DefaultValue("N/A") String nombre,
        @FormParam("apellidoPaterno") @DefaultValue("N/A") String apellidoPaterno,
        @FormParam("apellidoMaterno") @DefaultValue("N/A") String apellidoMaterno,
        @FormParam("calle") @DefaultValue("N/A") String calle,
        @FormParam("colonia") @DefaultValue("N/A") String colonia,
        @FormParam("numero") @DefaultValue("N/A") String numero,
        @FormParam("codigo_postal") @DefaultValue("0000") int codigo_postal,
        @FormParam("foto") @DefaultValue("N/A") String foto,
        @FormParam("fechaNacimiento") @DefaultValue("N/A") String fechaNacimiento,
        @FormParam("fechaInicio") @DefaultValue("N/A") String fechaInicio,
        @FormParam("genero") @DefaultValue("1") int genero,
        @FormParam("telefono") @DefaultValue("N/A") String telefono,
        
        @FormParam("usuario") @DefaultValue("N/A") String usuario,
        @FormParam("contrasenia") @DefaultValue("N/A") String contrasenia,       
        @FormParam("rol") @DefaultValue("N/A") String rol,
        
        @FormParam("nombre_ciudad") @DefaultValue("N/A") String nombre_ciudad,
        @FormParam("estado") @DefaultValue("N/A") String estado) throws Exception {
        
        String out = "";
            
        ControlGerente cg = new ControlGerente();
        
        Persona persona = new Persona(0, nombre, apellidoPaterno, apellidoMaterno, calle, colonia, numero, codigo_postal, foto, fechaNacimiento, fechaInicio, genero, telefono);
        
        UsuarioLogin usuarioLogin = new UsuarioLogin(0, usuario, contrasenia, "", 1, rol);
        
        Ciudad ciudad = new Ciudad(0, nombre_ciudad, estado);
        
        Gerente gerente = new Gerente(0, persona, usuarioLogin, ciudad);
        
            if(cg.insertGerente(gerente)){
                out = "{\"result\":\"OK\"}";
            }
            else {
                out = "{\"error\":\"ERROR\"}";
            }
       
        return Response.status(Response.Status.OK).entity(out).build();
    }
    
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(
        @FormParam("nombre") @DefaultValue("N/A") String nombre,
        @FormParam("apellidoPaterno") @DefaultValue("N/A") String apellidoPaterno,
        @FormParam("apellidoMaterno") @DefaultValue("N/A") String apellidoMaterno,
        @FormParam("calle") @DefaultValue("N/A") String calle,
        @FormParam("colonia") @DefaultValue("N/A") String colonia,
        @FormParam("numero") @DefaultValue("N/A") String numero,
        @FormParam("codigo_postal") @DefaultValue("00000") int codigo_postal,
        @FormParam("foto") @DefaultValue("N/A") String foto,
        @FormParam("fechaNacimiento") @DefaultValue("N/A") String fechaNacimiento,
        @FormParam("fechaInicio") @DefaultValue("N/A") String fechaInicio,
        @FormParam("genero") @DefaultValue("0000000000") int genero,
        @FormParam("telefono") @DefaultValue("N/A") String telefono,
        @FormParam("idGerente") @DefaultValue("0") int idGerente) throws Exception {
            
        ControlGerente cg = new ControlGerente();
        Persona persona =new Persona(0, nombre, apellidoPaterno, apellidoMaterno, calle, colonia, numero, codigo_postal, foto, fechaNacimiento, fechaInicio, genero, telefono);
        UsuarioLogin usuario = new UsuarioLogin(0, numero, colonia, foto, genero, foto);
        Ciudad ciudad = new Ciudad(0, nombre, foto);
        Gerente gerente = new Gerente(idGerente, persona, usuario, ciudad);
       String out = "";
        
       
            if(cg.updateGerente(gerente)){
                out = "{\"result\":\"OK\"}";
            }
            else {
                out = "{\"error\":\"ERROR\"}";
            }
       
        return Response.status(Response.Status.OK).entity(out).build();
    }
}
