                            /*Módulo del Gerente*/
var gerentesAct = null;
var gerentesIna = null;
var gerente = null;

function cargarListas(){
    //alert("Si entra");
    mostrarGerentesAct();
    mostrarGerentesIna();
    mostrarVendedoresAct();
    mostrarVendedoresIna();
}

function guardarGerente() {
    var nombre = $('#txtNombre').val();
    //alert(nombre);
    var apellidoPaterno = $('#txtApellidoPaterno').val();
    //alert(apellidoPaterno);
    var apellidoMaterno = $('#txtApellidoMaterno').val();
    //alert(apellidoMaterno);
    var calle = $('#txtCalle').val();
    //alert(apellidoMaterno);
    var colonia = $('#txtColonia').val();
    //alert(apellidoMaterno);
    var numero = $('#txtNumero').val();
    //alert(apellidoMaterno);
    var codigo_postal = $('#txtCodigo_postal').val();
    //alert(apellidoMaterno);
    var foto = $('#txtFoto').val();
    //alert(apellidoMaterno);
    var fechaNacimiento = $('#txtFechaNacimiento').val();
    //alert(fechaNac);
    var fechaInicio = $('#txtFechaInicio').val();
    //alert(fechaNac);
    var genero = $('#generoPer').val();
    //alert(genero);
    var telefono = $('#txtTelefono').val();
    //alert(telefono );
    var usuario = $('#txtUsuario').val();
    
    var contrasenia = $('#txtContrasenia').val();
    
    //var token = $('#txtToken').val();
    
    var rol = $('#txtRol').val();
    
    var nombre_ciudad = $('#txtCiudad').val();
    
    var estado = $('#txtEstado').val();
    
    var datos = {
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,
        calle: calle,
        colonia: colonia,
        numero: numero,
        codigo_postal: codigo_postal,
        foto: foto,
        fechaNacimiento: fechaNacimiento,
        fechaInicio: fechaInicio,
        genero: genero,
        telefono: telefono,
        usuario: usuario,
        contrasenia: contrasenia,
        //token: token,
        rol: rol,
        nombre_ciudad: nombre_ciudad,
        estado: estado
    };
    //alert(JSON.stringify(datos));
    $.ajax({
        type: "POST",
        async: true,
        url: "api/Gerente/insert",
        data: datos
    }
    ).done(function (data) {
        if (data.result != null) {
            //alert("¡Empleado agregado con exito!");
            swal("Good job!", "¡Gerente agregado con exito!", "success");
        } else {
            alert("No se pudo agregar el Gerente");
        }
        mostrarGerentesAct();
    }
    );
}

function mostrarGerentesAct() {
    $.ajax(
            {
                type: "GET",
                url: "api/Gerente/getAll",
                async: true
            }).done(
            function (data) {
                //alert(JSON.stringify(data));
                gerentesAct = data;
                var tabla = "<table class='table table-sm table-hover table-dark' id=tablaGerentesAct> \n\
                  <caption>Gerentes Activos</caption> ";
                tabla += "<tr class='bg-info'> <th scope=col>Nombre</th> <th scope=col>Apellidos</th> <th scope=col>Calle</th> <th scope=col>Colonia</th>\n\
                 <th scope=col>Numero</th> <th scope=col>Código Postal</th> <th scope=col>Foto</th> <th scope=col>Fecha de Nacimiento</th>\n\
                 <th scope=col>Fecha de Inicio</th> <th scope=col>Género</th> <th scope=col>Teléfono</th> <th scope=col>Usuario</th>\n\
                 <th scope=col>Rol</th> <th scope=col>Ciudad</th> <th scope=col>Estado</th> <th></th> </tr>";
                for (var i = 0; i < data.length; i++) {
                    tabla += "<tr scope=row>";
                    //alert(data[i].persona.nombre);
                    tabla += "<td>" + data[i].persona.nombre + "</td>";
                    tabla += "<td>" + data[i].persona.apellidoPaterno +" "+ data[i].persona.apellidoMaterno + "</td>";
                    tabla += "<td>" + data[i].persona.calle + "</td>";
                    tabla += "<td>" + data[i].persona.colonia + "</td>";
                    tabla += "<td>" + data[i].persona.numero + "</td>";
                    tabla += "<td>" + data[i].persona.codigo_postal + "</td>";
                    tabla += "<td> <img width='42' height='42' src='" + data[i].persona.foto + "' alt='Sin-Foto ' /> </td>";
                    tabla += "<td>" + data[i].persona.fechaNacimiento + "</td>";
                    tabla += "<td>" + data[i].persona.fechaInicio + "</td>";
                    if (data[i].persona.genero == '2') {
                        tabla += "<td> Mujer </td>";
                    } else {
                        if (data[i].persona.genero == '1') {
                            tabla += "<td> Hombre </td>";
                        } else {
                            tabla += "<td> Otro </td>";
                        }
                    }
                    tabla += "<td>" + data[i].persona.telefono + "</td>";
                    tabla += "<td>" + data[i].usuario.usuario + "</td>";
                    //tabla += "<td>" + data[i].usuario.contrasenia + "</td>";
                    //tabla += "<td>" + data[i].usuario.token + "</td>";
                    tabla += "<td>" + data[i].usuario.rol + "</td>";
                    tabla += "<td>" + data[i].ciudad.nombre_ciudad + "</td>";
                    tabla += "<td>" + data[i].ciudad.estado + "</td>";
                    
                    tabla += "<td> <button class='btn btn-outline-info' onclick='cargarModificarGerente( " + i + ") '>Modificar</button></td>";
                    tabla += "<td> <button class='btn btn-outline-danger' onclick='eliminarGerente( " + i + ") '>Eliminar</button></td>";
                    tabla += "</tr>";

                }
                                   
                tabla += "</table>";
                //alert(tabla);
                $('#tablaGerentesActi').html(tabla);
            });
}

function mostrarGerentesIna() {
    $.ajax(
            {
                type: "GET",
                url: "api/Gerente/getAllDelete",
                async: true
            }).done(
            function (data) {
                //alert(JSON.stringify(data));
                gerentesIna = data;
                var tabla = "<table class='table table-sm table-hover table-dark' id=tablaGerentesIna> \n\
                  <caption>Gerentes Inactivos</caption> ";
                tabla += "<tr class='bg-info'> <th scope=col>Nombre</th> <th scope=col>Apellidos</th> <th scope=col>Calle</th> <th scope=col>Colonia</th>\n\
                 <th scope=col>Numero</th> <th scope=col>Código Postal</th> <th scope=col>Foto</th> <th scope=col>Fecha de Nacimiento</th>\n\
                 <th scope=col>Fecha de Inicio</th> <th scope=col>Género</th> <th scope=col>Teléfono</th> <th scope=col>Usuario</th>\n\
                 <th scope=col>Rol</th> <th scope=col>Ciudad</th> <th scope=col>Estado</th> <th></th> </tr>";
                for (var i = 0; i < data.length; i++) {
                    tabla += "<tr scope=row>";
                    //alert(data[i].persona.nombre);
                    tabla += "<td>" + data[i].persona.nombre + "</td>";
                    tabla += "<td>" + data[i].persona.apellidoPaterno +" "+ data[i].persona.apellidoMaterno + "</td>";
                    tabla += "<td>" + data[i].persona.calle + "</td>";
                    tabla += "<td>" + data[i].persona.colonia + "</td>";
                    tabla += "<td>" + data[i].persona.numero + "</td>";
                    tabla += "<td>" + data[i].persona.codigo_postal + "</td>";
                    tabla += "<td> <img width='42' height='42' src='" + data[i].persona.foto + "' alt='Sin-Foto ' /> </td>";
                    tabla += "<td>" + data[i].persona.fechaNacimiento + "</td>";
                    tabla += "<td>" + data[i].persona.fechaInicio + "</td>";
                    if (data[i].persona.genero == '2') {
                        tabla += "<td> Mujer </td>";
                    } else {
                        if (data[i].persona.genero == '1') {
                            tabla += "<td> Hombre </td>";
                        } else {
                            tabla += "<td> Otro </td>";
                        }
                    }
                    tabla += "<td>" + data[i].persona.telefono + "</td>";
                    tabla += "<td>" + data[i].usuario.usuario + "</td>";
                    //tabla += "<td>" + data[i].usuario.contrasenia + "</td>";
                    //tabla += "<td>" + data[i].usuario.token + "</td>";
                    tabla += "<td>" + data[i].usuario.rol + "</td>";
                    tabla += "<td>" + data[i].ciudad.nombre_ciudad + "</td>";
                    tabla += "<td>" + data[i].ciudad.estado + "</td>";
                    
                    tabla += "<td> <button class='btn btn-outline-success' onclick='ActivarGerente( " + i + ") '>Reactivar</button></td>";
                    tabla += "</tr>";

                }
                                   
                tabla += "</table>";
                //alert(tabla);
                $('#tablaGerentesInac').html(tabla);
            });
}

function eliminarGerente(i) {
    var mm= gerentesAct[i].idGerente;
    var data={idGerente:mm};
    $.ajax({
        type: "POST",
        async: true,
        url: "api/Gerente/delete",
        data:data
    }
    ).done(function (data) {
        if (data.result != null){
        //    alert("No se pudo hacer el delate");
        }else{
        //   alert("si pudo ");
 }
        //alert("¡Eliminación exitosa del Empleado!");
        swal("Good job!", "¡Eliminación exitosa del Gerente!", "success");
        cargarModuloGerente();
    }
    );
}

function cargarModuloGerente() {
    $.ajax({
        type: "GET",
        url: "gerente.html",
        async: true
    }
    ).done(
            function (data)
            {
                mostrarGerentesAct();
                mostrarGerentesIna();
            }
    );
}

function ActivarGerente(i) {
    var mm = gerentesIna[i].idGerente;
    var data = {idGerente: mm};
    $.ajax({
        type: "POST",
        async: true,
        url: "api/Gerente/reactivate",
        data: data
    }
    ).done(function (data) {
        if (data.result != null) {
            //    alert("No se pudo hacer el delate");
        } else {
            //   alert("si pudo ");
        }

        // swal("¡Se ha reactivado correctamente el Empleado!");
        swal("Good job!", "¡Se ha reactivado correctamente el Gerente!", "success");

        cargarModuloGerente();
    }
    );
}

function buscarGerente() {
    var tableReg = document.getElementById('tablaGerentesAct');
    var gen = $('#searchGerente').val();
    var searchText = document.getElementById('searchGerente').value.toLowerCase();

    for (var i = 1; i < tableReg.rows.length; i++) {
        var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');

        var found = false;
        for (var j = 0; j < ((cellsOfRow.length) - 3) && !found; j++) {
            var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }

    var tableRegp = document.getElementById('tablaGerentesIna');
    var genp = $('#searchGerente').val();
   // var searchTextp = document.getElementById('searchEmpleado').value.toLowerCase();    
    for (var ip = 1; ip < tableRegp.rows.length; ip++) {
        var cellsOfRowp = tableRegp.rows[ip].getElementsByTagName('td');
        var foundp = false;
        for (var jp = 0; jp < ((cellsOfRowp.length) - 2) && !foundp; jp++) {
            var compareWithp = cellsOfRowp[jp].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWithp.indexOf(searchText) > -1)) {
                foundp = true;
            }
        }
        if (foundp) {
            tableRegp.rows[ip].style.display = '';
        } else {
            tableRegp.rows[ip].style.display = 'none';
        }
    }
}

function guardarModificarGerente() {  
    var idGerente=$('#txtIdGerente').val();
    
    var nombre = $('#txtNombre').val();
    //alert(nombre);
    var apellidoPaterno = $('#txtApellidoPaterno').val();
    //alert(apellidoPaterno);
    var apellidoMaterno = $('#txtApellidoMaterno').val();
    //alert(apellidoMaterno);
    var calle = $('#txtCalle').val();
    //alert(apellidoMaterno);
    var colonia = $('#txtColonia').val();
    //alert(apellidoMaterno);
    var numero = $('#txtNumero').val();
    //alert(apellidoMaterno);
    var codigo_postal = $('#txtCodigo_postal').val();
    //alert(apellidoMaterno);
    var foto = $('#txtFoto').val();
    //alert(apellidoMaterno);
    var fechaNacimiento = $('#txtFechaNacimiento').val();
    //alert(fechaNac);
    var fechaInicio = $('#txtFechaInicio').val();
    //alert(fechaNac);
    var genero = $('#generoPer').val();
    //alert(genero);
    var telefono = $('#txtTelefono').val();
    //alert(telefono );
    var info = {
        idGerente: idGerente,
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,
        calle: calle,
        colonia: colonia,
        numero: numero,
        codigo_postal: codigo_postal,
        foto: foto,
        fechaNacimiento: fechaNacimiento,
        fechaInicio: fechaInicio,
        genero: genero,
        telefono: telefono
    };
    $.ajax({
        type: "POST",
        async: true,
        url: "api/Gerente/update",
        data: info
    }
    ).done(function (data) {
        if (data.result != null){
           
            //alert("¡Actualización exitosa del Empleado!");
            swal("Good job!", "¡Actualización exitosa del Gerente!", "success");
        }else{
            alert("¡No se puedo actualizar el Gerente!");}
        mostrarGerentesAct();
    }
    );
}

function cargarModificarGerente(posicion) {
   
    gerente = gerentesAct[posicion]; 
    $.ajax({
        type: "GET",
        url: "gerente.html",
        async: true
    }
    ).done(
            function (data)
            {                
                $('#txtIdGerente').val(gerente.idGerente);
                
                $('#txtNombre').val(gerente.persona.nombre);
                $('#txtApellidoPaterno').val(gerente.persona.apellidoPaterno);
                $('#txtApellidoMaterno').val(gerente.persona.apellidoMaterno);
                $('#txtCalle').val(gerente.persona.calle);                
                $('#txtColonia').val(gerente.persona.colonia);
                $('#txtNumero').val(gerente.persona.numero);                
                $('#txtCodigo_postal').val(gerente.persona.codigo_postal);                
                $('#txtFoto').val(gerente.persona.foto);                
                $('#txtFechaNacimiento').val(gerente.persona.fechaNacimiento);               
                $('#txtFechaInicio').val(gerente.persona.fechaInicio);
                $('#txtTelefono').val(gerente.persona.telefono);
                
                if(gerente.persona.genero =="2"){
                    document.getElementById("generoPer").selectedIndex = "1";
                }else{
                    if(gerente.persona.genero =="1"){
                        document.getElementById("generoPer").selectedIndex = "0";
                    }else{
                        document.getElementById("generoPer").selectedIndex = "2";
                    }
                }                                    
            }
    );
}

function decidirGerente(){
    if (gerente== null){
        guardarGerente()();
    }else{
        guardarModificarGerente();      
    }
    limpiarCampos();
}

function encodeImagetoBase64(element) {
    var file = element.files[0];
    var reader = new FileReader();
    var base64 = "";
    
    reader.onloadend = function () {
        base64 = reader.result;
        $('#txtFoto').val(base64);
        $('#Imgfoto').attr("src", base64);
        $("#imgSala").show();
    };
    reader.readAsDataURL(file);
}

function limpiarCampos(){
    $('#txtNombre').val("");
    $('#txtApellidoPaterno').val("");
    $('#txtApellidoMaterno').val("");
    $('#txtCalle').val("");
    $('#txtColonia').val("");
    $('#txtNumero').val("");
    $('#txtCodigo_postal').val("");
    $('#txtFechaNacimiento').val("");
    $('#txtFechaInicio').val("");
    $('#generoPer').val("");
    $('#txtTelefono').val("");
}


/*-------------------------------------Módulo del Vendedor---------------------------*/
var vendedoresAct = null;
var vendedoresIna = null;
var vendedor = null;

function guardarVendedor() {
    var nombre = $('#txtNombre').val();
    //alert(nombre);
    var apellidoPaterno = $('#txtApellidoPaterno').val();
    //alert(apellidoPaterno);
    var apellidoMaterno = $('#txtApellidoMaterno').val();
    //alert(apellidoMaterno);
    var calle = $('#txtCalle').val();
    //alert(apellidoMaterno);
    var colonia = $('#txtColonia').val();
    //alert(apellidoMaterno);
    var numero = $('#txtNumero').val();
    //alert(apellidoMaterno);
    var codigo_postal = $('#txtCodigo_postal').val();
    //alert(apellidoMaterno);
    var foto = $('#txtFoto').val();
    //alert(apellidoMaterno);
    var fechaNacimiento = $('#txtFechaNacimiento').val();
    //alert(fechaNac);
    var fechaInicio = $('#txtFechaInicio').val();
    //alert(fechaNac);
    var genero = $('#generoPer').val();
    //alert(genero);
    var telefono = $('#txtTelefono').val();
    //alert(telefono );
    var usuario = $('#txtUsuario').val();
    
    var contrasenia = $('#txtContrasenia').val();
        
    var status = $('#txtEstatus').val();
    
    var rol = $('#txtRol').val();
    
    
    var nombre_ciudad = $('#txtCiudad').val();
    
    var estado = $('#txtEstado').val();
    
    var zona = $('#txtZona').val();
    
    var datos = {
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,
        calle: calle,
        colonia: colonia,
        numero: numero,
        codigo_postal: codigo_postal,
        foto: foto,
        fechaNacimiento: fechaNacimiento,
        fechaInicio: fechaInicio,
        genero: genero,
        telefono: telefono,
        usuario: usuario,
        contrasenia: contrasenia,
        status: status,
        rol: rol,
        nombre_ciudad: nombre_ciudad,
        estado: estado,
        zona: zona
    };
    //alert(JSON.stringify(datos));
    $.ajax({
        type: "POST",
        async: true,
        url: "api/vendedor/insert",
        data: datos
    }
    ).done(function (data) {
        if (data.result != null) {
            //alert("¡Empleado agregado con exito!");
            swal("Good job!", "¡Vendedor agregado con exito!", "success");
        } else {
            alert("No se pudo agregar el Vendedor");
        }
        mostrarVendedoresAct()();
    }
    );
}

function mostrarVendedoresAct() {
    $.ajax(
            {
                type: "GET",
                url: "api/vendedor/getAll",
                async: true
            }).done(
            function (data) {
                //alert(JSON.stringify(data));
                vendedoresAct = data;
                var tabla = "<table class='table table-sm table-hover table-dark' id=tablaVendedoresAct> \n\
                  <caption>Vendedores Activos</caption> ";
                tabla += "<tr class='bg-info'> <th scope=col>Nombre</th> <th scope=col>Apellidos</th> <th scope=col>Calle</th> <th scope=col>Colonia</th>\n\
                 <th scope=col>Numero</th> <th scope=col>Código Postal</th> <th scope=col>Foto</th> <th scope=col>Fecha de Nacimiento</th>\n\
                 <th scope=col>Fecha de Inicio</th> <th scope=col>Género</th> <th scope=col>Teléfono</th> <th scope=col>Usuario</th>\n\
                 <th scope=col>Rol</th> <th scope=col>Zona</th> <th></th> </tr>";
                for (var i = 0; i < data.length; i++) {
                    tabla += "<tr scope=row>";
                    //alert(data[i].persona.nombre);
                    tabla += "<td>" + data[i].persona.nombre + "</td>";
                    tabla += "<td>" + data[i].persona.apellidoPaterno +" "+ data[i].persona.apellidoMaterno + "</td>";
                    tabla += "<td>" + data[i].persona.calle + "</td>";
                    tabla += "<td>" + data[i].persona.colonia + "</td>";
                    tabla += "<td>" + data[i].persona.numero + "</td>";
                    tabla += "<td>" + data[i].persona.codigo_postal + "</td>";
                    tabla += "<td> <img width='42' height='42' src='" + data[i].persona.foto + "' alt='Sin-Foto ' /> </td>";
                    tabla += "<td>" + data[i].persona.fechaNacimiento + "</td>";
                    tabla += "<td>" + data[i].persona.fechaInicio + "</td>";
                    if (data[i].persona.genero == '2') {
                        tabla += "<td> Mujer </td>";
                    } else {
                        if (data[i].persona.genero == '1') {
                            tabla += "<td> Hombre </td>";
                        } else {
                            tabla += "<td> Otro </td>";
                        }
                    }
                    tabla += "<td>" + data[i].persona.telefono + "</td>";
                    tabla += "<td>" + data[i].usuario.usuario + "</td>";
                    //tabla += "<td>" + data[i].usuario.contrasenia + "</td>";
                    //tabla += "<td>" + data[i].usuario.token + "</td>";
                    tabla += "<td>" + data[i].usuario.rol + "</td>";
                    
                    tabla += "<td>" + data[i].zonas.nombre_zona + "</td>";
                    
                    tabla += "<td> <button class='btn btn-outline-info' onclick='cargarModificarVendedor( " + i + ") '>Modificar</button></td>";
                    tabla += "<td> <button class='btn btn-outline-danger' onclick='eliminarVendedor( " + i + ") '>Eliminar</button></td>";
                    tabla += "</tr>";

                }
                                   
                tabla += "</table>";
                //alert(tabla);
                $('#tablaVendedoresActi').html(tabla);
            });
}

function mostrarVendedoresIna() {
    $.ajax(
            {
                type: "GET",
                url: "api/vendedor/getAllDelete",
                async: true
            }).done(
            function (data) {
                //alert(JSON.stringify(data));
                vendedoresIna = data;
                var tabla = "<table class='table table-sm table-hover table-dark' id=tablaVendedoresIna> \n\
                  <caption>Vendedores Activos</caption> ";
                tabla += "<tr class='bg-info'> <th scope=col>Nombre</th> <th scope=col>Apellidos</th> <th scope=col>Calle</th> <th scope=col>Colonia</th>\n\
                 <th scope=col>Numero</th> <th scope=col>Código Postal</th> <th scope=col>Foto</th> <th scope=col>Fecha de Nacimiento</th>\n\
                 <th scope=col>Fecha de Inicio</th> <th scope=col>Género</th> <th scope=col>Teléfono</th> <th scope=col>Usuario</th>\n\
                 <th scope=col>Rol</th> <th scope=col>Zona</th> <th></th> </tr>";
                for (var i = 0; i < data.length; i++) {
                    tabla += "<tr scope=row>";
                    //alert(data[i].persona.nombre);
                    tabla += "<td>" + data[i].persona.nombre + "</td>";
                    tabla += "<td>" + data[i].persona.apellidoPaterno +" "+ data[i].persona.apellidoMaterno + "</td>";
                    tabla += "<td>" + data[i].persona.calle + "</td>";
                    tabla += "<td>" + data[i].persona.colonia + "</td>";
                    tabla += "<td>" + data[i].persona.numero + "</td>";
                    tabla += "<td>" + data[i].persona.codigo_postal + "</td>";
                    tabla += "<td> <img width='42' height='42' src='" + data[i].persona.foto + "' alt='Sin-Foto ' /> </td>";
                    tabla += "<td>" + data[i].persona.fechaNacimiento + "</td>";
                    tabla += "<td>" + data[i].persona.fechaInicio + "</td>";
                    if (data[i].persona.genero == '2') {
                        tabla += "<td> Mujer </td>";
                    } else {
                        if (data[i].persona.genero == '1') {
                            tabla += "<td> Hombre </td>";
                        } else {
                            tabla += "<td> Otro </td>";
                        }
                    }
                    tabla += "<td>" + data[i].persona.telefono + "</td>";
                    tabla += "<td>" + data[i].usuario.usuario + "</td>";
                    //tabla += "<td>" + data[i].usuario.contrasenia + "</td>";
                    //tabla += "<td>" + data[i].usuario.token + "</td>";
                    tabla += "<td>" + data[i].usuario.rol + "</td>";
                    
                    tabla += "<td>" + data[i].zonas.nombre_zona + "</td>";
                    
                    tabla += "<td> <button class='btn btn-outline-success' onclick='ActivarVendedor( " + i + ") '>Reactivar</button></td>";
                    tabla += "</tr>";

                }
                                   
                tabla += "</table>";
                //alert(tabla);
                $('#tablaVendedoresInac').html(tabla);
            });
}

function eliminarVendedor(i) {
    var mn= vendedoresAct[i].idVendedor;
    var data={idVendedor:mn};
    $.ajax({
        type: "POST",
        async: true,
        url: "api/vendedor/delete",
        data:data
    }
    ).done(function (data) {
        if (data.result != null){
        //    alert("No se pudo hacer el delate");
        }else{
        //   alert("si pudo ");
 }
        //alert("¡Eliminación exitosa del Empleado!");
        swal("Good job!", "¡Eliminación exitosa del Vendedor!", "success");
        cargarModuloVendedor();
    }
    );
}

function cargarModuloVendedor() {
    $.ajax({
        type: "GET",
        url: "vendedor.html",
        async: true
    }
    ).done(
            function (data)
            {
                mostrarVendedoresAct();
                mostrarVendedoresIna();
            }
    );
}

function ActivarVendedor(i) {
    var mn = vendedoresIna[i].idVendedor;
    var data = {idVendedor: mn};
    $.ajax({
        type: "POST",
        async: true,
        url: "api/vendedor/active",
        data: data
    }
    ).done(function (data) {
        if (data.result != null) {
            //    alert("No se pudo hacer el delate");
        } else {
            //   alert("si pudo ");
        }

        // swal("¡Se ha reactivado correctamente el Empleado!");
        swal("Good job!", "¡Se ha reactivado correctamente el Vendedor!", "success");
        cargarModuloVendedor();
    }
    );
}

function buscarVendedor() {
    var tableReg = document.getElementById('tablaVendedoresAct');
    var gen = $('#searchVendedor').val();
    var searchText = document.getElementById('searchVendedor').value.toLowerCase();

    for (var i = 1; i < tableReg.rows.length; i++) {
        var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');

        var found = false;
        for (var j = 0; j < ((cellsOfRow.length) - 3) && !found; j++) {
            var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }

    var tableRegp = document.getElementById('tablaVendedoresIna');
    var genp = $('#searchVendedor').val();
   // var searchTextp = document.getElementById('searchEmpleado').value.toLowerCase();    
    for (var ip = 1; ip < tableRegp.rows.length; ip++) {
        var cellsOfRowp = tableRegp.rows[ip].getElementsByTagName('td');
        var foundp = false;
        for (var jp = 0; jp < ((cellsOfRowp.length) - 2) && !foundp; jp++) {
            var compareWithp = cellsOfRowp[jp].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWithp.indexOf(searchText) > -1)) {
                foundp = true;
            }
        }
        if (foundp) {
            tableRegp.rows[ip].style.display = '';
        } else {
            tableRegp.rows[ip].style.display = 'none';
        }
    }
}

function guardarModificarVendedor() {  
    var idPersona=$('#txtIdPersona').val();
    
    var nombre = $('#txtNombre').val();
    //alert(nombre);
    var apellidoPaterno = $('#txtApellidoPaterno').val();
    //alert(apellidoPaterno);
    var apellidoMaterno = $('#txtApellidoMaterno').val();
    //alert(apellidoMaterno);
    var calle = $('#txtCalle').val();
    //alert(apellidoMaterno);
    var colonia = $('#txtColonia').val();
    //alert(apellidoMaterno);
    var numero = $('#txtNumero').val();
    //alert(apellidoMaterno);
    var codigo_postal = $('#txtCodigo_postal').val();
    //alert(apellidoMaterno);
    var foto = $('#txtFoto').val();
    //alert(apellidoMaterno);
    var fechaNacimiento = $('#txtFechaNacimiento').val();
    //alert(fechaNac);
    var fechaInicio = $('#txtFechaInicio').val();
    //alert(fechaNac);
    var genero = $('#generoPer').val();
    //alert(genero);
    var telefono = $('#txtTelefono').val();
    //alert(telefono );
    var info = {
        idPersona: idPersona,
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,
        calle: calle,
        colonia: colonia,
        numero: numero,
        codigo_postal: codigo_postal,
        foto: foto,
        fechaNacimiento: fechaNacimiento,
        fechaInicio: fechaInicio,
        genero: genero,
        telefono: telefono
    };
    $.ajax({
        type: "POST",
        async: true,
        url: "api/vendedor/update",
        data: info
    }
    ).done(function (data) {
        if (data.result != null){
           
            //alert("¡Actualización exitosa del Empleado!");
            swal("Good job!", "¡Actualización exitosa del Vendedor!", "success");
        }else{
            alert("¡No se puedo actualizar el Vendedor!");}
        mostrarVendedoresAct()();
    }
    );
}

function cargarModificarVendedor(posicion) {
   
    vendedor = vendedoresAct[posicion]; 
    $.ajax({
        type: "GET",
        url: "vendedor.html",
        async: true
    }
    ).done(
            function (data)
            {                
                $('#txtIdPersona').val(vendedor.persona.idPersona);
                
                $('#txtNombre').val(vendedor.persona.nombre);
                $('#txtApellidoPaterno').val(vendedor.persona.apellidoPaterno);
                $('#txtApellidoMaterno').val(vendedor.persona.apellidoMaterno);
                $('#txtCalle').val(vendedor.persona.calle);                
                $('#txtColonia').val(vendedor.persona.colonia);
                $('#txtNumero').val(vendedor.persona.numero);                
                $('#txtCodigo_postal').val(vendedor.persona.codigo_postal);                
                $('#txtFoto').val(vendedor.persona.foto);                
                $('#txtFechaNacimiento').val(vendedor.persona.fechaNacimiento);               
                $('#txtFechaInicio').val(vendedor.persona.fechaInicio);
                $('#txtTelefono').val(vendedor.persona.telefono);
                
                if(vendedor.persona.genero =="2"){
                    document.getElementById("generoPer").selectedIndex = "1";
                }else{
                    if(vendedor.persona.genero =="1"){
                        document.getElementById("generoPer").selectedIndex = "0";
                    }else{
                        document.getElementById("generoPer").selectedIndex = "2";
                    }
                }                                    
            }
    );
}

function decidirVendedor(){
    if (vendedor== null){
        guardarVendedor()()();
    }else{
        guardarModificarVendedor()();      
    }
    limpiarCampos();
}                                